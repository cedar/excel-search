# Connect to remote Jupyter notebooks
server
```
jupyter notebook --no-browser --port=8889 --ip=127.0.0.1
```
(take note of the access token)

local
```
./open_notebooks.sh
```

# Collect `TableMetadata` from ttl files
```
cd src/scripts
python collect_table_metadata.py
```
a pickle file which stores all collected `TableMetadata` objects will be generated at `data/all_tablemetadata.pkl`

# Install Redis
```
sudo yum install redis
sudo systemctl start redis
```

Redis configurations
+ Configuration file `/etc/redis.conf`
+ Log file `/var/log/redis/redis.log`

Redis server working directory
```
redis-cli CONFIG SET dir /data/tcao/redis
chmod 777 /data/tcao/redis
```

# Setup
pip
TreeTagger
Create logs folder

# Download France's geonames data
curl -O http://download.geonames.org/export/dump/FR.zip

# Run Flask app in localhost
kill $(lsof -t -i:8080)
ssh -N -f -L localhost:8080:localhost:5000 tcao@cedar007.saclay.inria.fr

kill $(lsof -t -i:8080)
ssh -N -f -L localhost:8080:localhost:5000 tcao@node7.oak.saclay.inria.fr

# Install Python 3 in Centos
sudo yum -y update
sudo yum -y install yum-utils
sudo yum -y groupinstall development
sudo yum install -y https://centos6.iuscommunity.org/ius-release.rpm (for Centos 6)
sudo yum install -y https://centos7.iuscommunity.org/ius-release.rpm (for Centos 7)
sudo yum install -y python36u python36u-libs python36u-devel python36u-pip



# RDF server
tdbloader --loc <database folder> --graph=<named graph> <path to RDF file>
e.g:
tdbloader --loc insee_rdf --graph=http://inseeXtr.excel/data/tcao/insee_data/2673683/multiple_BASE_TD_FILO_DEC_IRIS_2013/0/0.ttl /data/tcao/insee_data/2673683/multiple_BASE_TD_FILO_DEC_IRIS_2013/0/0.ttl
./fuseki-server --update --loc <database folder> /<server endpoint name>
e.g:
java -Xmx40G -jar fuseki-server.jar --update --loc /data/tcao/insee_rdf /insee

# Query RDF server with named graph
prefix inseeXtr: <http://inseeXtr.excel/>
prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
select * where {
    graph <http://insee.xtr/data/tcao/insee/2128766/multiple_dep58/4/0.ttl>
    {
      ?header inseeXtr:value ?header_value .
      ?header rdf:type inseeXtr:HeaderRow
    }
}

# Cronjob to collect daily tweets
0 * * * * /home/cedar/tcao/excel-search/src/scripts/daily_tweets.sh
