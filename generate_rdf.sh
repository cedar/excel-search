# Stop Fuseki if necessary
kill $(lsof -t -i:3030)

home_directory=/home/cedar/tcao

if [ "$1"  = "--dev" ]; then
  # Delete existing ttl files list otherwise new files will be appended
  rm /data/tcao/xls_ttl_files.txt
  rm /data/tcao/html_ttl_files.txt

  # Excel extractor
  cd $home_directory/excel-extractor
  source .env/bin/activate
  python main.py --run multiple 3 --dev
  deactivate

  # HTML extractor
  cd $home_directory/excel-search
  source .env/bin/activate
  cd src
  sh extract_html_files.sh --dev

  # tdbloader
  insee_rdf=$home_directory/insee_rdf_dev
else
  # Excel extractor
  cd $home_directory/excel-extractor
  source .env/bin/activate
  python main.py --run multiple
  deactivate

  # HTML extractor
  cd $home_directory/excel-search
  source .env/bin/activate
  cd src
  sh extract_html_files.sh

  # tdbloader
  insee_rdf=$home_directory/insee_rdf
fi

rm -rf $insee_rdf
mkdir $insee_rdf
python -m scripts.update_rdf_server $insee_rdf |& tee  $home_directory/excel-search/logs/tdbloader.log

# start Fuseki and run document_score script
cd $FUSEKI
sh launch_insee_rdf.sh $insee_rdf |& tee rdf_server.log &
# Wait for the server
sleep 10
cd $home_directory/excel-search/src
sh document_score.sh
