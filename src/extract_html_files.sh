if [ "$1"  = "--dev" ]; then
  python -m scripts.extract_html_tables /home/cedar/tcao/insee-crawler/html_extractor_input.csv 10 --random-test
else
  python -m scripts.extract_html_tables /home/cedar/tcao/insee-crawler/html_extractor_input.csv
fi
