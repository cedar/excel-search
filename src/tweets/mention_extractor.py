import os
import pickle
from datetime import datetime as dt
from webapp.app import logger
from tweets.entity_context_extractor import extract_entity_and_context, nlp
from tweets.date_time_extractor import extract_datetime
from tweets.tweet_extractor import tweets_from_file, collect_tweets
from tweets.tweet_preprocessor import get_candidate


all_terms = set([term.strip() for term in \
        open('resources/unit_entities.txt').read().splitlines()])


def __highlighted(text, color):
    return f'<mark style="background-color: {color}">{text}</mark>'


def html_and_query(nodes, context):
    doc = nodes[0].doc
    context_node_ids = list()
    for node in context[1:]:
        if hasattr(node, 'i'):
            context_node_ids.append(node.i)
        else:
            context_node_ids.extend(list(range(node.start, node.end)))

    query_items = list()
    if nodes[-1].pos_ == 'NUM':
        html_texts = list()
        for node in doc:
            if node.i == nodes[0].i:
                html_texts.append(__highlighted(node.text, 'green'))
                query_items.append(node.text)
            elif node.i == nodes[-1].i or node.i == nodes[-2].i:
                html_texts.append(__highlighted(node.text, 'red'))
            elif node.i in context_node_ids:
                html_texts.append(__highlighted(node.text, 'cyan'))
                query_items.append(node.text)
            else:
                html_texts.append(node.text)

        return ' '.join(html_texts), ' '.join(query_items)

    return doc.text, None


def extract_from_tweet_data(tweet_data, published_date, term):
    tweet, name, link = tweet_data
    results = list()
    for sentence_ in nlp(tweet).sents:
        sentence = sentence_.text
        if term not in sentence:
            continue
        try:
            context, nodes, _ = extract_entity_and_context(sentence, term.split(' ')[0])
            if nodes:
                html, query = html_and_query(nodes, context)

                dates = extract_datetime(sentence, published_date)
                if dates:
                    for date_category, _date in dates:
                        if date_category == 'DATE':
                            query += ' ' + _date

                results.append({
                        'published_date': published_date,
                        'tweet': tweet,
                        'html': html,
                        'query': query,
                        'name': name,
                        'link': link
                })
        except Exception as e:
            print(e)

    return results


def extract_from_candidate2(published_date, \
        tweets_folder='/data/tcao/daily_tweets_politique1/', \
        members_list_file='politique1_user_names.csv'):
    tweets_folder = tweets_folder + published_date
    counter = 0
    matched = 0

    all_links = set()
    with open(members_list_file) as f:
        for line in f.read().splitlines():
            member, name = line.split(',')
            file_path = f'{tweets_folder}/{member}.json'
            if not os.path.exists(file_path):
                continue

            for tweet in tweets_from_file(file_path):
                tweet_data, published_date = tweet

                _, _, link = tweet_data
                if link in all_links:
                    continue
                else:
                    all_links.add(link)

                tweet_data, published_date = get_candidate(tweet)
                if tweet_data:
                    tweet, name, link = tweet_data
                    lowered_tweet = tweet.lower()
                    for term in all_terms:
                        if term in lowered_tweet:
                            matched += 1
                            results = extract_from_tweet_data(tweet_data, published_date, term)
                            if results:
                                for data in results:
                                    counter += 1
                                    logger.info(f'Term {term} => data = {data}')
                                    yield {'data': data, 'progress': f'{counter}/{matched}'}


def extract_from_candidate(since, until, \
        tweets_folder='/data/tcao/tweets_politique1/', \
        members_list_file='politique1_user_names.csv'):
    since_date = dt.strptime(since, "%Y-%m-%d")
    until_date = dt.strptime(until, "%Y-%m-%d")
    counter = 0
    matched = 0

    all_links = set()
    with open(members_list_file) as f:
        for line in f.read().splitlines():
            member, name = line.split(',')
            file_path = f'{tweets_folder}{member}.json'
            if not os.path.exists(file_path):
                continue

            for tweet in tweets_from_file(file_path):
                tweet_data, published_date = tweet

                _, _, link = tweet_data
                if link in all_links:
                    continue
                else:
                    all_links.add(link)

                _published_date = dt.strptime(published_date, "%Y-%m-%d")
                if _published_date < since_date or until_date < _published_date:
                    continue

                tweet_data, published_date = get_candidate(tweet)
                if tweet_data:
                    tweet, name, link = tweet_data
                    lowered_tweet = tweet.lower()
                    for term in all_terms:
                        if term in lowered_tweet:
                            matched += 1
                            results = extract_from_tweet_data(tweet_data, published_date, term)
                            if results:
                                for data in results:
                                    counter += 1
                                    logger.info(f'Term {term} => data = {data}')
                                    yield {'data': data, 'progress': f'{counter}/{matched}'}
