import json
import os
import subprocess
from datetime import timedelta, datetime


def collect_tweets(since, until, \
        tweets_folder='/data/tcao/tweets_politique1', \
        members_list_file='politique1_user_names.csv'):
    with open(members_list_file) as f:
        os.system(f'mkdir -p {tweets_folder}\n')
        for line in f.read().splitlines():
            member, name = line.split(',')
            print(f'Collect tweets from {member}')
            os.system(f'twint -u {member} -o {tweets_folder}/{member}.json --json --since {since} --until {until}')
            file_path = f'{tweets_folder}/{member}.json'
            if os.path.exists(file_path):
                length = len(open(file_path).read().splitlines())
            else:
                length = 0
            yield {'username': member, 'length': length}


def collect_tweets2(since, until):
    since = datetime.strptime(since, '%Y-%m-%d')
    until = datetime.strptime(until, '%Y-%m-%d')
    day = since
    while day <= until:
        _day = day.strftime('%Y-%m-%d')
        collect_daily_tweets(_day)
        day = day + timedelta(days=1)


def collect_daily_tweets(day, \
        tweets_folder='/data/tcao/daily_tweets_politique1', \
        members_list_file='politique1_user_names.csv'):
    folder = os.path.join(tweets_folder, day)
    print('Collect daily tweets', day)
    os.system(f'mkdir {folder}')
    next_day = datetime.strptime(day, "%Y-%m-%d") + timedelta(days=1)
    next_day = next_day.strftime('%Y-%m-%d')
    with open(members_list_file) as f:
        for line in f.read().splitlines():
            member, name = line.split(',')
            os.system(f'twint -u {member} -o {folder}/{member}.json --json --since {day} --until {next_day}')
            file_path = f'{tweets_folder}/{member}.json'
            if os.path.exists(file_path):
                length = len(open(file_path).read().splitlines())
            else:
                length = 0


def tweet_from_json(_json):
    dictionary = json.loads(_json)
    return (dictionary['tweet'], dictionary['name'], dictionary['link']), dictionary['date']


def tweets_from_file(json_file):
    with open(json_file) as f:
        for _json in f.read().splitlines():
            try:
                yield tweet_from_json(_json)
            except:
                pass


def tweets_from_date(_date, tweets_json_folder='/data/tcao/list_politique1/'):
    for json_file in os.listdir(tweets_json_folder):
        for tweet_data, published_date in tweets_from_file(tweets_json_folder + json_file):
            if published_date == _date:
                yield tweet_data


if __name__ == '__main__':
    tweet = tweet_from_json('''{"id": 1149006243241037824, "conversation_id": "1149006243241037824", "created_at": 1562779410000, "date": "2019-07-10", "time": "19:23:30", "timezone": "CEST", "user_id": 866717602298691584, "username": "zivkapark", "name": "Zivka Park", "place": null, "tweet": "Déception quant à l'issue de cette CMP: un beau travail collectif gâché et l'intérêt de nos concitoyens sacrifié pour des guéguerres politiques LR. #loimobilités @LaREM_AN @DamienPichereau @AN_DevDur @barbarapompili @BrunoMillienne @Jean_LucFUGIT @b_abba @BCouillard33 @jmzulesi pic.twitter.com/e4OLt5ICe2", "mentions": ["larem_an", "damienpichereau", "an_devdur", "barbarapompili", "brunomillienne", "jean_lucfugit", "b_abba", "bcouillard33", "jmzulesi"], "urls": [], "photos": ["https://pbs.twimg.com/media/D_IXEo2WwAEAQ1U.jpg", "https://pbs.twimg.com/media/D_IXFQcXUAA2_Mf.jpg"], "replies_count": 0, "retweets_count": 13, "likes_count": 24, "location": "", "hashtags": ["#loimobilités"], "link": "https://twitter.com/zivkapark/status/1149006243241037824", "retweet": null, "quote_url": "", "video": 0}''')
    print(tweet)
