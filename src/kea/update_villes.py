import codecs
from config.file_path import GEONAMES_FILE, KEA_VILLES_LIST


if __name__ == '__main__':
    with open(GEONAMES_FILE) as f:
        geonames = f.read().splitlines()
    with open(KEA_VILLES_LIST) as f:
        kea_villes = f.read().splitlines()

    with codecs.open(KEA_VILLES_LIST, 'a+', encoding='utf-8') as f:
        for new_name in (name for name in geonames if name not in kea_villes):
            f.write(new_name + '\n')
