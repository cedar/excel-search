import ast
import copy
import json
import os
from config import redis_conf

import redis

from algorithm.data_cell_search2 import the_most_relevant_data_cell
from algorithm.document_ranking_params import (HEADER_KEYWORD_COUNTING_MODE,
                                               LIST_G1_FUNCTIONS,
                                               LIST_G2_FUNCTIONS,
                                               WORD_SCORE_MODE)
from algorithm.fagin import fagin
from redis_collections import Dict
from utils import text_utils
from utils.geonames_utils import get_hierarchy
from word_document_score import (LOCATION_COMMENT, LOCATION_HEADER_COLUMN,
                                 LOCATION_HEADER_ROW, LOCATION_TITLE,
                                 NUM_SIMILAR_WORDS, VERSION, location_from,
                                 word_document_location_json_folder)

# r_ttl_file_paths = redis.StrictRedis(host='localhost', port=6379,
                                     # db=redis_conf.REDIS_DBS['ttl_file_paths'])
r_ttl_file_paths = redis.StrictRedis(host='127.0.0.1', port=6379,
                                     db=redis_conf.REDIS_DBS['ttl_file_paths'])


def _load_dict_from(directory, key):
    file_path = directory + key + '.json'
    if os.path.exists(file_path):
        with open(file_path) as f:
            return json.load(f)
    return dict()


def _load_data(word, json_folder):
    dict_hierarchy = dict()
    # if text_utils._is_geoname(word):
    # TODO re-enable loading similar words from geoname's hierarchy
    # hierarchy = get_hierarchy(word)
    # for (index, geoname) in enumerate(hierarchy):
    # print('Add document_location', geoname)
    # score_modifier = 1.0 - (index + 1) * 0.1
    #
    # original_dict = _load_dict_from(json_folder, geoname)
    # modified_dict = dict()
    # for d, location_score in original_dict.items():
    #     modified_dict[d] = \
    #         {l: s * score_modifier for l, s in location_score.items()}
    #
    # dict_hierarchy.update(modified_dict)

    dict_scores = _load_dict_from(json_folder, word)
    return {**dict_scores, **dict_hierarchy}


def load_document_score2(word, word_score_mode):
    document_location = _load_data(word, word_document_location_json_folder)

    document_score = dict()
    for document, location_score in document_location.items():
        if word_score_mode == 'max':
            document_score[document] = max(location_score.values())
        elif word_score_mode == 'sum':
            title_score = document_location.get('T', 0)
            comment_score = document_location.get('C', 0)

            dict_header_row_scores = {location: score for location, score
                                      in document_location.items() if 'HR' in location}
            header_row_score = max(
                dict_header_row_scores.values()) if dict_header_row_scores else 0

            dict_header_column_scores = {location: score for location, score
                                         in document_location.items() if 'HC' in location}
            header_column_score = max(
                dict_header_column_scores.values()) if dict_header_row_scores else 0

            document_score[document] = title_score + comment_score + \
                header_row_score + header_column_score
        else:
            assert 'word_score_mode has only 2 valid values: sum, max'

    return document_score, document_location


def load_document_location(word):
    return _load_data(word, word_document_location_json_folder)


def __get_dict(parent_dictionary, key, default):
    if key not in parent_dictionary:
        parent_dictionary[key] = default
    return parent_dictionary[key]


def get_word_document_score(keyword, word_score_mode):
    document_score, document_location = load_document_score2(
        keyword, word_score_mode)
    if '_' in keyword:
        lemma_form = text_utils.phrase_to_lemma(keyword)
    else:
        lemma_form = text_utils.word_to_lemma(keyword)

    if lemma_form != keyword:
        lemma_document_score, lemma_document_location = load_document_score2(
            lemma_form, word_score_mode)
        document_score.update(lemma_document_score)
    return document_score, document_location


def final_fagin_dictionaries2(search_terms, documents,
                              document_locations, header_keyword_counting_mode,
                              g1_scores):
    ''' {document: {
            "T": {search_term: score},
            "C": {search_term: score},
            "HR": {search_term: score},
            "HC": {search_term: score}
        }} '''
    document_dictionaries = dict()

    dictionaries_with_header_position = copy.deepcopy(document_locations)

    for document in documents:
        for search_term in search_terms:
            if document in document_locations[search_term]:
                dict_location_score = document_locations[search_term][document]
                for (location, score) in dict_location_score.items():
                    if document not in document_dictionaries:
                        document_dictionaries[document] = dict()
                    dict_document = document_dictionaries[document]

                    if 'T' == location or 'C' == location:
                        location_code = location
                    else:
                        location_code = location_from(location)

                    if location_code not in dict_document:
                        dict_document[location_code] = dict()
                    dict_document[location_code][search_term] = score

    dict_scores = {
        't': dict(),
        'c': dict(),
        'hr': dict(),
        'hc': dict(),
        'H': dict(),
        'g1': dict()
    }
    for document, data in document_dictionaries.items():
        dict_scores['g1'][document] = g1_scores[document]
        dict_scores['t'][document] = list(data.get('T', dict()).values())
        dict_scores['c'][document] = list(data.get('C', dict()).values())
        dict_scores['hr'][document] = list(data.get('HR', dict()).values())
        dict_scores['hc'][document] = list(data.get('HC', dict()).values())

        # prioritize having different keywords in headers
        dict_header_row = data.get('HR', dict())
        dict_header_column = data.get('HC', dict())
        dict_headers = {**dict_header_row, **dict_header_column}

        if header_keyword_counting_mode == 'data_cells_or_0':
            if len(dict_header_row) > 0 and len(dict_header_column) > 0 and \
                    set(dict_header_row.keys()) - dict_header_column.keys() != set():
                dict_scores['H'][document] = list(dict_headers.values())
            else:
                dict_scores['H'][document] = list()
        elif header_keyword_counting_mode == 'unique_keywords':
            dict_scores['H'][document] = list(dict_headers.values())
        else:
            assert 'header_keyword_counting_mode has only 2 valid values: data_cells_or_0, unique_keywords'

    return [dict_scores['t'],
            dict_scores['c'],
            dict_scores['hr'],
            dict_scores['hc'],
            dict_scores['H'],
            dict_scores['g1']], dictionaries_with_header_position


def g1(list_word_scores):
    num_keywords = sum(
        [1 for word_score in list_word_scores if word_score > 0])
    return sum(list_word_scores) + 10 ** num_keywords


def g2(arg):
    title_score, header_score, num_header_keywords = arg
    return title_score + header_score + 10 ** num_header_keywords


def search(query, k, p,
           g1,
           g2,
           word_score_mode='max',
           header_keyword_counting_mode='data_cells_or_0',
           contains_mandatory_keywords=False):
    ''' The most relevant documents of a query

    Args:
      query (str): the query
      k (int): number of documents we want to return
      p (int): `k * p` is the number of documents return by Fagin with function `g1`
      g1 : global score function of word document score and number of matching keywords
      g2 : global score function of title score, header score number of matching keywords

    Returns:
      a list of `k` documents, each item of this list is a dictionary with the following items:
        list_location_score_dict: kth item contains dictionary of matching positions
            (title, headers or comment) and theirs corresponding scores
        global_scores (list of float): list of scores for computing `g2`
        document_id (int): ID of this document
        score (float): score returned by `g2`
        ttl_file (string): file path of the RDF file
    '''
    search_terms = list(text_utils.tokenize(
        query, remove_stop_words=True, lower=False, need_clean_accents=True, identify_place_name=True))
    search_terms = [t.lower() for t in search_terms]
    # import pdb; pdb.set_trace()

    list_dictionaries = list()
    dict_document_locations = dict()
    for term in search_terms:
        document_score, document_location = get_word_document_score(
            term, word_score_mode)
        list_dictionaries.append(document_score)
        dict_document_locations[term] = document_location

    num_empty_dict = sum((1 for d in list_dictionaries if d == {}))
    if num_empty_dict == len(list_dictionaries):
        # There are only empty dictionaries so
        # we can't search for anything
        return list()

    if contains_mandatory_keywords:
        g0 = lambda scores: scores[0] * scores[-1]
        # prep_fagin_results = fagin(len(list_dictionaries[0]), list_dictionaries, g0)
        prep_fagin_results = fagin(k * p * 100, list_dictionaries, g0)
        valid_document_ids = [d for (d, s) in prep_fagin_results if s == 1]
        for i in range(len(list_dictionaries)):
            dictionary = list_dictionaries[i]
            list_dictionaries[i] = {d : s for d, s in dictionary.items() if d in valid_document_ids}

    # for dic in list_dictionaries:
        # print(len(dic))

    num_docs = min(k * p, max(len(dic) for dic in list_dictionaries))
    first_fagin_results = fagin(num_docs, list_dictionaries, g1)
    documents = [d for (d, s) in first_fagin_results]
    g1_scores = {d: s for (d, s) in first_fagin_results}
    second_fagin_dictionaries, cache = final_fagin_dictionaries2(
        search_terms, documents,
        dict_document_locations, header_keyword_counting_mode, g1_scores)

    list_document_score = fagin(
        k, second_fagin_dictionaries, g2, tuple_input=True)

    ''' {'document_id', 'list_location_score_dict', 'score', 'ttl_file'} '''
    results = list()
    for document_id, score in list_document_score:
        if score > 0:
            list_location_score_dict = list()
            for search_term in search_terms:
                if document_id in cache[search_term]:
                    dict_location_score = cache[search_term][document_id]
                else:
                    dict_location_score = dict()
                list_location_score_dict.append(dict_location_score)

            results.append({
                'list_location_score_dict': list_location_score_dict,
                'global_scores': '',
                'document_id': document_id,
                'score': score,
                'ttl_file': r_ttl_file_paths.get(document_id).decode('utf-8')
            })

    return results
