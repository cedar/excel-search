'''
Implement Fagin Threshold algorithm
'''


def __dictionary_to_sorted_list(dictionary):
    return [(key, dictionary[key]) for
            key in sorted(dictionary, key=dictionary.get, reverse=True)]


def fagin(k, list_dictionaries, global_score_function, tuple_input=False):
    '''
    Args:
        k (int): number of most relevant documents should be returned
        list_dictionaries: list of dictionaries, each dictionary contains {document_id: score}
        global_score_function: a function over n individual scores

    Return:
        k most relevant document_id
    '''
    results = dict()

    n_sorted_list = [__dictionary_to_sorted_list(
        dictionary) for dictionary in list_dictionaries]

    num_search_terms = len(n_sorted_list)

    while True:
        top_document_scores = list()
        for i in range(num_search_terms):
            sorted_list = n_sorted_list[i]

            # don't pick document that exists in results already
            while True:
                if len(sorted_list) == 0:
                    top_document, score_i = None, 0
                    top_document_scores.append(score_i)
                    break

                top_document, score_i = sorted_list.pop(0)
                if top_document not in results:
                    top_document_scores.append(score_i)
                    break

            if top_document is not None:
                # all scores of top_document
                individual_scores = list()
                for j in range(num_search_terms):
                    if j != i:
                        individual_scores.append(
                            list_dictionaries[j].get(top_document, 0))
                    else:
                        individual_scores.append(score_i)

                if tuple_input:
                    global_score = global_score_function(*individual_scores)
                else:
                    global_score = global_score_function(individual_scores)

                # update results
                if len(results) < k:
                    results[top_document] = global_score
                else:
                    if global_score > min(results.values()):
                        del results[min(results, key=results.get)]
                        results[top_document] = global_score

        # compute threshold
        if tuple_input:
            threshold = global_score_function(*top_document_scores)
        else:
            threshold = global_score_function(top_document_scores)
        if len(results) >= k:
            if min(results.values()) >= threshold:
                return __dictionary_to_sorted_list(results)
