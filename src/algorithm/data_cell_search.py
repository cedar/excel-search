import time

from utils.rdf_utils import get_header_hierarchy
from webapp.app import logger
from word_document_score import (LOCATION_HEADER_COLUMN, LOCATION_HEADER_ROW,
                                 location_from)

DATA_ROW_LOCATION = 'data_row'
DATA_COLUMN_LOCATION = 'data_column'


def __get_dict(parent_dictionary, key, default):
    if key not in parent_dictionary:
        parent_dictionary[key] = default
    return parent_dictionary[key]


def pair_headers(keyword_position_in_headers):
    ''' Find all pair of headers which have the maximum number of keywords

    Args:
       keyword_position_in_headers {location: [word_index]}:
           the IDs of search terms for each location
           `location` (str): location of a header in form `HR#n` or `HC#n`
           `[word_index]` (list int): IDs of search terms that appear in parent header
               of `location`

    Returns:
      [(header_a , header_b)]: list of pair of headers
        header_a (str): location of a header in form `HR#n` or `HC#n`
        header_b (str): location of a header in form `HR#n` or `HC#n`
    '''

    ''' {word_index: list locations} '''
    word_to_headers = dict()
    for location, list_word_index in keyword_position_in_headers.items():
        for word_index in list_word_index:
            if word_index not in word_to_headers:
                word_to_headers[word_index] = list()
            word_to_headers[word_index].append(location)

    set_location_1, set_location_2 = set(), set()

    for location, list_word_index in keyword_position_in_headers.items():
        for word_index in [w for w in word_to_headers.keys() if w not in list_word_index]:
            set_location_1.add(location)
            set_location_2.update(word_to_headers[word_index])

    return set_location_1, set_location_2


def dictionary_keyword_position_in_headers(list_location_score_dict,
                                           ttl_file):
    '''
    Args:
        list_location_score_dict [{location : score}]:
            each item of this list contains scores of a corresponding search term
            `location` (str): the place where a keyword appears, possible values are
                `T` (title), `C` (comment), `HR#n` (header row), `HC#n` (header column)
            `score` (float): word score of search term
        header_hierarchy {child_location: parent_location}
            `child_location` (str): has the value `HR#n` or `HC#n`
            `parent_location` (str): has the value `HR#n` or `HC#n`, `parent_location`
                is the parent header of `child_location` in header hierarchy
    Return:
        keyword_position_in_headers_dict {location: [word_index]}:
            the IDs of search terms for each location
        dict_parent_headers {child_location: [word_index]}:
            `child_location` (str): location of a child header
            `[word_index]` (list int): IDs of search terms that appear in parent header
                of `child_location`
    '''
    dictionary = dict()

    for word_index, location_score_dict in enumerate(list_location_score_dict):
        for location in location_score_dict.keys():
            if LOCATION_HEADER_ROW in location or LOCATION_HEADER_COLUMN in location:
                list_word_index = __get_dict(dictionary,
                                             location,
                                             list())
                list_word_index.append(word_index)

    logger.debug('num matching headers {} {}'.format(
        ttl_file, len(dictionary)))

    # logger.debug('num pair of headers {}'.format(pair_headers(dictionary)))

    now = time.time()
    set_location_1, set_location_2 = pair_headers(dictionary)
    logger.debug('set 1 len {}, set 2 len {}'.format(
        len(set_location_1), len(set_location_2)))
    header_hierarchy = get_header_hierarchy(
        ttl_file,
        [h for h in set_location_1 if LOCATION_HEADER_ROW in h],
        [h for h in set_location_2 if LOCATION_HEADER_ROW in h],
        [h for h in set_location_1 if LOCATION_HEADER_COLUMN in h],
        [h for h in set_location_2 if LOCATION_HEADER_COLUMN in h],
    )
    logger.debug(
        'time retrieving header hierarchy {}'.format(time.time() - now))

    dict_parent_headers = dict()
    for location, list_word_index in dictionary.items():
        if location in header_hierarchy:
            parent_location = header_hierarchy[location]
            dict_parent_headers[location] = dictionary[parent_location]

    return dictionary, dict_parent_headers, header_hierarchy


def the_most_relevant_data_cell(list_location_score_dict, ttl_file):
    '''
    Args:
        list_location_score_dict [{location : score}]:
            each item of this list contains scores of a corresponding search term
            `location` (str): the place where a keyword appears, possible values are
                `T` (title), `C` (comment), `HR#n` (header row), `HC#n` (header column)
            `score` (float): word score of search term
        header_hierarchy {child_location: parent_location}
            `child_location` (str): has the value `HR_n` or `HC_n`
            `parent_location` (str): has the value `HR_n` or `HC_n`, `parent_location`
                is the parent header of `child_location` in header hierarchy
    Returns:
        hr (str): position of header row, or `None`
        hc (str): position of header column, or `None`
        keyword_positions (set int): IDs of search terms that correspond the the
            most relevant data cell, or `None`
    '''
    keyword_position_in_headers_dict, dict_parent_headers, header_hierarchy = \
        dictionary_keyword_position_in_headers(
            list_location_score_dict, ttl_file)
    header_columns = {DATA_COLUMN_LOCATION: []}
    header_rows = {DATA_ROW_LOCATION: []}
    for location, keyword_positions in keyword_position_in_headers_dict.items():
        positions = keyword_positions
        # Add keywords from parent header to its children header
        if location in dict_parent_headers:
            positions.extend(dict_parent_headers[location])

        if LOCATION_HEADER_ROW in location:
            header_rows[location] = positions
        if LOCATION_HEADER_COLUMN in location:
            header_columns[location] = positions

    data_cells = dict()
    for hr, hr_kw_positions in header_rows.items():
        for hc, hc_kw_positions in header_columns.items():
            data_cells[(hr, hc)] = set(hr_kw_positions + hc_kw_positions)

    if data_cells:
        sorted_tuples = sorted(data_cells.items(),
                               key=lambda t: len(t[1]), reverse=True)
        (hr, hc), set_keyword_positions = sorted_tuples[0]

        new_header_hierarchy = dict()
        for key, value in header_hierarchy.items():
            new_header_hierarchy[key.replace(
                '_', '#')] = value.replace('_', '#')

        if hr == DATA_ROW_LOCATION:
            row_results = [[]]
        else:
            row_results = [get_all_parent_headers(
                header, new_header_hierarchy) for header in get_children_headers(new_header_hierarchy, hr)]
        if hc == DATA_COLUMN_LOCATION:
            col_results = [[]]
        else:
            col_results = [get_all_parent_headers(
                header, new_header_hierarchy) for header in get_children_headers(new_header_hierarchy, hc)]
        return row_results, col_results, set_keyword_positions
    return None, None, None


def get_children_headers(header_hierarchy, parent_header):
    children_headers = list()
    for child, parent in header_hierarchy.items():
        if parent == parent_header:
            children_headers.append(child)

    grand_children_headers = list()
    for child_header in children_headers:
        grand_children_headers.extend(get_children_headers(
            header_hierarchy, child_header))

    children_headers.extend(grand_children_headers)

    if not children_headers:
        return [parent_header]
    return list(set(children_headers))


def get_all_parent_headers(child_header, header_hierarchy):
    all_parent_headers = list()
    parent_header = header_hierarchy.get(child_header, None)
    while parent_header:
        all_parent_headers.append(parent_header)
        parent_header = header_hierarchy.get(parent_header, None)

    return list(reversed([child_header] + all_parent_headers))
