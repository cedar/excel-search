import copy
import sys


def hierarchy(below_row, above_row):
    '''
    Args:
        below_row (list (int, int)): list of tuples (cell_id, colspan)
        above_row (list (int, int)): list of tuples (cell_id, colspan)
    Returns:
        {int: int}: dictionary of {child_cell_id: parent_row_id}
    '''
    dict_hierarchy = dict()

    def process_next_parent_cell():
        if len(above_row) > 0:
            parent_cell_id, parent_cell_colspan = above_row.pop(0)
            return parent_cell_id, parent_cell_colspan, 0
        else:
            return None, None, None

    parent_cell_id, parent_cell_colspan, sum_colspan = process_next_parent_cell()

    for (cell_id, colspan) in below_row:
        if sum_colspan + colspan > parent_cell_colspan:
            parent_cell_id, parent_cell_colspan, sum_colspan = process_next_parent_cell()
            sum_colspan = colspan
        else:
            sum_colspan += colspan

        if sum_colspan <= parent_cell_colspan:
            if cell_id != parent_cell_id:
                dict_hierarchy[cell_id] = parent_cell_id

    return dict_hierarchy


def header_rows_hierarchy(list_rows):
    '''
    Args:
        list_rows (list `row`): the first row is the top header row
        row (list (int, int)): list of tuples (cell_id, colspan)
    Returns:
        {int: int}: dictionary of {child_cell_id: parent_row_id}
    '''
    dict_hierarchy = dict()

    for index, _ in enumerate(list_rows):
        # The header cells in the top have no parent headers
        if index == 0:
            for (cell_id, _) in list_rows[index]:
                dict_hierarchy[cell_id] = None

        if index < len(list_rows) - 1:
            dict_hierarchy.update(hierarchy(
                above_row=list_rows[index], below_row=list_rows[index + 1]
            ))

    return dict_hierarchy


def header_columns_hierarchy(list_rows):
    '''
    Args:
        list_rows (list `row`): the first row has the first data row
        row (list int): list of header cell id
    Returns:
        {int: int}: dictionary of {child_cell_id: parent_row_id}
    '''
    dict_hierarchy = dict()

    # Number of cells in each row
    num_cells = len(list_rows[0])

    for row in list_rows:
        for index, cell_id in enumerate(row):
            if index == 0:
                dict_hierarchy[cell_id] = None
            if index + 1 < num_cells:
                dict_hierarchy[row[index + 1]] = cell_id

    return dict_hierarchy
