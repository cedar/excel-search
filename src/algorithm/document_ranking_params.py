

P_VALUES = [3]

''' NOTE: We only use word score mode "max" '''
WORD_SCORE_MODE = ['sum', 'max']

LIST_G1_FUNCTIONS = {
    'g1.1': lambda list_word_scores: sum(list_word_scores) + 10 ** sum(list_word_scores),
    'g1.2': lambda list_word_scores: sum(list_word_scores),
    'g1.3': lambda list_word_scores: 10 ** len(list_word_scores),
    'g1.4': lambda list_word_scores: 10 ** sum(list_word_scores),
}

HEADER_KEYWORD_COUNTING_MODE = ['data_cells_or_0', 'unique_keywords']


def experimental_g2(t, c, hr, hc, H, g1):
    if len(t) == 0:
        return 0
    return 10 ** sum(t) + 5 ** sum(hr) + 5 ** sum(hc) + 7 ** sum(H) + 3 ** sum(c) + g1


'''
t: title scores,
c: comment scores,
hr: header row scores,
hc: header column scores,
H: header scores (only unique keywords in both headers),
'''
LIST_G2_FUNCTIONS = {
    'g2.1': lambda t, c, hr, hc, H, g1:  sum(t) + sum(H) + 10 ** len(H),
    'g2.2': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(H) + 10 ** len(H),

    'g2.3': lambda t, c, hr, hc, H, g1:  sum(t) + sum(H) + sum(c) + 10 ** len(H),
    'g2.4': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(H) + 10 ** sum(c) + 10 ** len(H),

    'g2.5': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(hr) + 10 ** sum(hc) + 10 ** sum(H) + 10 ** sum(c) + 10 ** len(H),
    'g2.6': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(hr) + 10 ** sum(hc) + 10 ** sum(H) + 10 ** (sum(c) / max(len(c), 1)) + 10 ** len(H),

    'g2.7': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(H) + 10 ** (len(H) + len(t)),
    'g2.7.1': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(H) + 10 ** (sum(H) + sum(t)),
    'g2.7.2': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(H) + 10 ** (sum(H) + sum(t)) + 10 ** sum(c),
    'g2.7.3': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(H) + 10 ** (sum(H) + sum(t)) + 5 ** sum(c),
    'g2.7.4': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(H) + 8 ** (sum(H) + sum(t)),
    'g2.7.5': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(H) + 8 ** (sum(H) + sum(t)) + 5 ** sum(c),
    'g2.8': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(H) + 10 ** (len(H) + len(t)) + 10 ** (sum(c) / max(len(c), 1)),
    'g2.8.1': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(H) + 10 ** (sum(H) + sum(t)) + 10 ** (sum(c) / max(len(c), 1)),
    'g2.9': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(H) + 10 ** (len(H) + len(t)) + 10 ** sum(c),

    'g2.10': lambda t, c, hr, hc, H, g1:  10 ** len(t) + 10 ** len(H) + 10 ** len(c),
    'g2.11': lambda t, c, hr, hc, H, g1:  10 ** len(t) + 10 ** len(H) + 10 ** len(c) + 10 ** len(hc) + 10 ** len(hr),
    'g2.12': lambda t, c, hr, hc, H, g1:  10 ** len(t) + 10 ** len(H) + 10 ** (len(c) - 1),
    'g2.13': lambda t, c, hr, hc, H, g1:  10 ** len(t) + 10 ** (len(H) + 1) + 10 ** (len(c) - 1),

    'g2.5.dev': experimental_g2,
    'g2.5.dev1': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 10 ** sum(hr) + 10 ** sum(hc) + 10 ** sum(H) + 10 ** sum(c) + 10 ** len(H) + g1,
    'g2.5.dev2': lambda t, c, hr, hc, H, g1:  10 ** sum(t) + 5 ** sum(hr) + 5 ** sum(hc) + 7 ** sum(H) + 3 ** sum(c) + g1,

    'g2.14.1': lambda t, c, hr, hc, H, g1:  10 ** len(t) + 10 ** len(hr) + 10 ** len(hc) + 10 ** len(H) + 10 ** len(c) + 10 ** len(H) + g1,
    'g2.14.2': lambda t, c, hr, hc, H, g1:  10 ** len(t) + 5 ** len(hr) + 5 ** len(hc) + 7 ** len(H) + 3 ** len(c) + g1,
}
