from utils import rdf_utils
import time


def find_num_keywords(dict_header_row_keywords, candidate_headers):
    keywords = set()
    for header in candidate_headers:
        keywords.update(dict_header_row_keywords[header])
    return len(keywords)


def find_relevant_headers(dict_header_keyword):
    ''' Find the data cells which contain the maximum number of keywords in their corresponding headers

    Args:
      dict_header_keyword ({header_id: keyword_ids}): mapping of header and its keywords
        `header_id` (str): ID of header in form 'HR#n' or 'HC#n'
        `keyword_ids` (list int): list of keyword ID

    Returns:
      pair of headers to which the data cells with the maximum number of keywords belong
    '''

    # print()
    new_dict_header_row_keywords = dict()
    for header, keywords in dict_header_keyword.items():
        desired_keywords = set(keywords)
        if desired_keywords:
            new_dict_header_row_keywords[header] = desired_keywords

    ''' all keywords appear in header row '''
    header_row_keywords = set()
    ''' {keyword_id: set headers} '''
    keyword_to_headers = dict()
    for header, keywords in new_dict_header_row_keywords.items():
        desired_keywords = set(keywords)
        header_row_keywords.update(desired_keywords)
        for keyword in desired_keywords:
            if keyword not in keyword_to_headers:
                keyword_to_headers[keyword] = set()
            keyword_to_headers[keyword].add(header)

    candidate_header_rows = list()
    for header, keywords in new_dict_header_row_keywords.items():
        new_candidates = list()
        new_candidates.append(set([header]))
        # Search for other headers
        # the combination of these headers could contain more keywords
        missing_keywords = header_row_keywords - keywords
        for missing_keyword in missing_keywords:
            available_headers = keyword_to_headers.get(
                missing_keyword, set())

            # we should pick a header that isn't overlap with other headers in the candidate
            new_headers = [new_header for new_header in available_headers if new_dict_header_row_keywords[new_header].intersection(
                new_dict_header_row_keywords[header]
            ) == set()]

            if new_headers:
                import copy
                current_candidates = copy.deepcopy(new_candidates)
                new_candidates.clear()
                for new_header in new_headers:
                    for candidate in current_candidates:
                        new_candidate = copy.deepcopy(candidate)
                        new_candidate.add(new_header)
                        new_candidates.append(new_candidate)

        for candidate in new_candidates:
            if candidate not in candidate_header_rows:
                candidate_header_rows.append(candidate)

    dict_num_keywords = dict()
    dict_removable_candidates = dict()
    for candidate_id, candidate in enumerate(candidate_header_rows):
        dict_removable_candidates[candidate_id] = False
        dict_num_keywords[candidate_id] = find_num_keywords(
            dict_header_keyword, candidate)

    if dict_num_keywords:
        max_num_keywords_of_candidate = max(dict_num_keywords.values())
    else:
        max_num_keywords_of_candidate = 0

    for candidate_id, candidate in enumerate(candidate_header_rows):
        if dict_num_keywords[candidate_id] < max_num_keywords_of_candidate:
            dict_removable_candidates[candidate_id] = True
        else:
            duplicated_candidate = False
            for existing_candidate in candidate_header_rows:
                if existing_candidate != candidate:
                    if candidate.intersection(existing_candidate) == existing_candidate:
                        duplicated_candidate = True
                        break
            if duplicated_candidate:
                dict_removable_candidates[candidate_id] = True

    return [candidate_header_rows[candidate_id] for candidate_id, removable in
            dict_removable_candidates.items() if not removable]


def find_header_keyword(list_location_score_dict):
    ''' List of keywords IDs for each header

    Args:
        list_location_score_dict [{location : score}]:
            each item of this list contains scores of a corresponding search term
            `location` (str): the place where a keyword appears, possible values are
                `T` (title), `C` (comment), `HR#n` (header row), `HC#n` (header column)
            `score` (float): word score of search term
    Return:
        keyword_position_in_headers_dict {location: [word_index]}:
            the IDs of search terms for each location
    '''
    dictionary = dict()

    for word_index, location_score_dict in enumerate(list_location_score_dict):
        for location in location_score_dict.keys():
            if 'HR' in location or 'HC' in location:
                list_word_index = __get_dict(dictionary,
                                             location,
                                             list())
                list_word_index.append(word_index)

    return dictionary


def __get_dict(parent_dictionary, key, default):
    if key not in parent_dictionary:
        parent_dictionary[key] = default
    return parent_dictionary[key]


def the_most_relevant_data_cell(list_location_score_dict, ttl_file):
    '''
    Args:
        list_location_score_dict [{location : score}]:
            each item of this list contains scores of a corresponding search term
            `location` (str): the place where a keyword appears, possible values are
                `T` (title), `C` (comment), `HR#n` (header row), `HC#n` (header column)
            `score` (float): word score of search term
        header_hierarchy {child_location: parent_location}
            `child_location` (str): has the value `HR_n` or `HC_n`
            `parent_location` (str): has the value `HR_n` or `HC_n`, `parent_location`
                is the parent header of `child_location` in header hierarchy
    Returns:
        hr (str): position of header row, or `None`
        hc (str): position of header column, or `None`
        keyword_positions (set int): IDs of search terms that correspond the the
            most relevant data cell, or `None`
    '''
    header_keyword = find_header_keyword(list_location_score_dict)
    relevant_headers = find_relevant_headers(header_keyword)

    header_row_ids = list()
    header_column_ids = list()
    for headers in relevant_headers:
        new_headers = [h.replace('#', '_') for h in headers]

        set_header_row_ids = set([h for h in new_headers if 'HR' in h])
        if set_header_row_ids not in header_row_ids:
            header_row_ids.append(set_header_row_ids)

        set_header_column_ids = set([h for h in new_headers if 'HC' in h])
        if set_header_column_ids not in header_column_ids:
            header_column_ids.append(set_header_column_ids)

    # Query hierarchy
    selected_header_row_ids = rdf_utils.get_header_hierarchy2(
        ttl_file, header_row_ids, 'row')
    selected_header_column_ids = rdf_utils.get_header_hierarchy2(
        ttl_file, header_column_ids, 'column')
    print('num selected_header_row_ids row {}'.format(
        len(selected_header_row_ids)))
    print('num selected_header_column_ids column {}'.format(
        len(selected_header_column_ids)))

    now = time.time()
    results = rdf_utils.get_data_cell2(' '.join(selected_header_row_ids),
                                       ' '.join(selected_header_column_ids))
    print('Query data cell in {} seconds'.format(time.time() - now))
    return results
