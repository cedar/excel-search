from datetime import datetime
from tweets import tweet_extractor

days = '''2019-08-24
2019-08-25
2019-08-26
2019-08-27
2019-08-28
2019-08-29
2019-08-30
2019-08-31
2019-09-01
2019-09-03'''
for day in days.split('\n'):
    tweet_extractor.collect_daily_tweets(day)
