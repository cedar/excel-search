import copy
import os
import random
import re
import sys
from config import constants

import dill as pickle
from bs4 import BeautifulSoup

from algorithm.html_table_hierarchy import (header_columns_hierarchy,
                                            header_rows_hierarchy)
from data_models import DataSet, Sheet
from utils import soup_utils, text_utils, worker_pool
from utils.log_utils import get_log
from utils.rdf_converter import RdfConverter

DEBUG = False

INSEE_ID_REGEX = '(?<={})(.*?)(?=/)'.format(constants.INSEE_DATA_FOLDER)

TABLE_URL_PATTERN = 'https://www.insee.fr/fr/statistiques/{}#{}'

LOG_FILE = '../logs/extract_html_tables.log'
logger = get_log(LOG_FILE, overwrite=True)


def catch_error(f):
    def func_wrapper(*args, **kwargs):
        try:
            f(*args, **kwargs)
        except Exception:
            downloaded_date, url, file_path, description, published_date = args[0]
            logger.exception('Failed to extract file: ' + file_path)
    return func_wrapper


def get_clean_text(html_element):
    return text_utils.clean_space_characters(html_element.get_text())


def extract_header_row_hierarchy(all_trs):
    '''
    Args:
        all_trs: list of all <tr> tags
    Returns:
        {int: int}: dictionary {child_header_id: parent_header_id}
        {int: str}: dictionary {header_id: header_value}
        [int]: list of header cell id in the last row
    '''
    dict_header_cells = dict()
    hierarchy = dict()

    all_table_rows = [list() for row_index, _ in enumerate(all_trs)]
    header_cell_id = 0

    # These duplicated cells should be added to the end of new row
    # {row_index: [(header_cell_id, colspan)]}
    dict_duplicated_cells = dict()

    def add_new_header_cell(th, colspan, header_cell_id,
                            row_index,
                            increase_header_cell_id=True,
                            add_to_the_end_of_row=False):
        row = all_table_rows[row_index]
        header = th.get_text().strip()
        dict_header_cells[header_cell_id] = header

        if add_to_the_end_of_row:
            if row_index not in dict_duplicated_cells:
                dict_duplicated_cells[row_index] = list()

            dict_duplicated_cells[row_index].append(
                (header_cell_id, colspan))
        else:
            row.append((header_cell_id, colspan))

        return header_cell_id + 1 if increase_header_cell_id else header_cell_id

    for row_index, tr in enumerate(all_trs):
        table_row = all_table_rows[row_index]

        all_ths = tr.find_all('th')

        for cell_index, th in enumerate(all_ths):
            if th is not None:
                if 'colspan' in th.attrs:
                    colspan = int(th.attrs['colspan'])
                else:
                    colspan = 1

                if 'rowspan' in th.attrs:
                    if cell_index > 0 or row_index > 0:
                        num_rows = int(th.attrs['rowspan'])
                        # This situation happens when the tag
                        # "rowspan" has the wrong value
                        if row_index + num_rows >= len(all_table_rows):
                            num_rows = max(
                                0, len(all_table_rows) - row_index)

                        for row_offset in range(num_rows):
                            # split this cell into several cells
                            # add these cells to the corresponding rows
                            row = all_table_rows[row_index + row_offset]
                            header_cell_id = add_new_header_cell(
                                th, colspan, header_cell_id,
                                row_index + row_offset, False,
                                cell_index == len(all_ths) - 1)
                        header_cell_id += 1
                    continue

                if row_index == len(all_trs) - 1:
                    # the bottom row of header rows should be
                    # splitted into many cells with colspan=1
                    for i in range(colspan):
                        header_cell_id = add_new_header_cell(
                            th, 1, header_cell_id, row_index)
                else:
                    header_cell_id = add_new_header_cell(
                        th, colspan, header_cell_id, row_index)

        if row_index in dict_duplicated_cells:
            table_row.extend(dict_duplicated_cells[row_index])

    last_header_row_ids = [header_cell_id for (
        header_cell_id, colspan) in all_table_rows[-1]]
    hierarchy.update(header_rows_hierarchy(all_table_rows))

    return hierarchy, dict_header_cells, last_header_row_ids


def extract_header_column_hierarchy(all_trs, num_header_columns):
    '''
    Args:
        all_trs: list of all <tr> tags
        num_header_columns (int)
    Returns:
        {int: int}: dictionary {child_header_id: parent_header_id}
        {int: str}: dictionary {header_id: header_value}
        [int]: list of header cell IDs in the last column
        [[str]]: each item is a list of data cell's values
    '''
    dict_header_cells = dict()
    rows = [list() for _ in enumerate(all_trs)]
    data_rows = list()
    header_cell_id = 0

    def add_new_header_cell(cell, row_index, header_cell_id,
                            increase_header_cell_id=True):
        rows[row_index].append(header_cell_id)

        if header_cell_id not in dict_header_cells:
            dict_header_cells[header_cell_id] = cell.get_text().strip()

        return header_cell_id + 1 if increase_header_cell_id else header_cell_id

    def is_big_header_cell(tr):
        if len(tr) > 1:
            only_empty_cells = True
            # The first cell is the big header and all the rest are empty
            for cell in tr[1:]:
                if get_clean_text(cell) != '':
                    only_empty_cells = False
                    break
            return only_empty_cells
        return len(tr) == 1

    # Big header cell is a cell which occupies the whole row
    big_header_cell_id = None

    for row_index, tr in enumerate(all_trs):
        has_big_header_cell = False

        for cell_index, cell in enumerate(tr[:num_header_columns]):
            # The big cell should be parent header of next row's header cells
            if is_big_header_cell(tr):
                big_header_cell_id = header_cell_id
                dict_header_cells[header_cell_id] = cell.get_text().strip()
                header_cell_id += 1

                has_big_header_cell = True
                break

            if cell_index == 0:
                if big_header_cell_id != None:
                    rows[row_index].append(big_header_cell_id)

            # divide cell with "rowspan" into small cells
            # and put them into next rows
            if 'rowspan' in cell.attrs:
                rowspan = int(cell.attrs['rowspan'])
                if rowspan > 1:
                    for offset in range(rowspan):
                        header_cell_id = add_new_header_cell(
                            cell,
                            row_index + offset,
                            header_cell_id,
                            increase_header_cell_id=False
                        )
                    header_cell_id += 1
            else:
                header_cell_id = add_new_header_cell(
                    cell, row_index, header_cell_id)

        if not has_big_header_cell:
            if len(tr[num_header_columns:]) > 0:
                data_rows.append([get_clean_text(cell)
                                  for cell in tr[num_header_columns:]])

    if is_big_header_cell(all_trs[0]):
        num_header_columns += 1
    # Remove empty row of big header cell
    rows = [row[:num_header_columns] for row in rows if row != []]

    assert len(rows) == len(data_rows)

    last_column_cell_ids = [row[-1] for row in rows]

    return header_columns_hierarchy(rows), dict_header_cells, last_column_cell_ids, data_rows


def get_num_header_columns(header_row_trs):
    first_row = header_row_trs[0]

    rows = first_row.find_all()
    if len(rows) == 1:
        return 1
    else:
        first_cell = rows[0]
        return int(soup_utils.get_attribute(first_cell, 'colspan', 1))


def get_trs(soup):
    '''
    Args:
        soup: BeautifulSoup object
    Returns:
        list of <tr> tags: each tag is a row in header rows
        list of list ResultSet objects: each list ResultSet objects is a row in header columns
    '''
    header_row_trs = list()
    header_column_trs = list()

    def get_cell_tags(tr):
        # return tr.find_all()
        return tr.find_all(lambda tag: 'class' in tag.attrs or tag.name == 'th' or tag.name == 'td')

    in_header_row_zone = True
    all_trs = soup.find_all('tr')
    for tr in all_trs:
        if not in_header_row_zone:
            header_column_trs.append(get_cell_tags(tr))
        else:
            is_header_row = False
            for cell_index, cell in enumerate(get_cell_tags(tr)):
                if cell_index != 0 and soup_utils.get_attribute(cell, 'scope', '') == 'col':
                    is_header_row = True
                    break

            if is_header_row:
                header_row_trs.append(tr)
            else:
                header_column_trs.append(get_cell_tags(tr))

    if len(header_row_trs) == 0:
        header_row_trs = [all_trs[0]]
        header_column_trs = header_column_trs[1:]
    return header_row_trs, header_column_trs


@catch_error
def extract(arg):
    downloaded_date, url, file_path, description, published_date = arg

    ttl_file = file_path.replace('.html', '.ttl')

    with open(file_path) as f:
        html = f.read()
        soup = BeautifulSoup(html, 'html.parser')

    url = ''
    if soup.figure:
        regex_search = re.search(INSEE_ID_REGEX, file_path)
        if regex_search:
            insee_id = regex_search.group()
        else:
            insee_id = ''
        table_id = soup_utils.get_attribute(soup.figure, 'id', None)
        url = TABLE_URL_PATTERN.format(
            insee_id,
            table_id if table_id is not None else ''
        )

    title = ''
    if soup.caption:
        title = get_clean_text(soup.caption)

    unit_information = soup.find(class_="unite")
    if unit_information:
        title += ' ' + get_clean_text(unit_information)

    comment_tag = soup.find(class_="notes")
    comment = get_clean_text(comment_tag) if comment_tag is not None else ''

    rdf_converter = RdfConverter(ttl_file,
                                 DataSet(url, downloaded_date,
                                         file_path, description),
                                 Sheet(0, title,
                                       comment,
                                       ttl_file, published_date))

    header_row_trs, header_column_trs = get_trs(soup)
    num_header_columns = get_num_header_columns(header_row_trs)

    hierarchy, dict_header_cells, last_row_cell_ids = extract_header_row_hierarchy(
        header_row_trs)
    if is_debug_mode():
        print(dict_header_cells)
        print(hierarchy)

    rdf_converter.write_header_rows(hierarchy, dict_header_cells)

    header_column_hierarchy, dict_header_cells, last_column_cell_ids, data_rows = extract_header_column_hierarchy(
        header_column_trs, num_header_columns)
    if is_debug_mode():
        print(dict_header_cells)
        print(header_column_hierarchy)

    rdf_converter.write_header_columns(
        header_column_hierarchy, dict_header_cells)

    rdf_converter.write_data_rows(
        data_rows, last_row_cell_ids[-len(data_rows[0]):], last_column_cell_ids)

    with open(constants.HTML_TTL_FILES, 'a+') as f:
        f.write('{}\n'.format(ttl_file))

    logger.info(
        'Extracting done {}'.format(file_path))


def extract_html_tables(list_args):
    logger.info('Extract {} files'.format(len(list_args)))
    worker_pool.parallel(extract, list_args, 0.5, 10000, logger)


def is_debug_mode():
    return '--debug' in sys.argv


def is_random_test():
    return '--random-test' in sys.argv


def get_file_path(url, file_name):
    if '?' in url:
        insee_id = url[url.rindex('/') + 1: url.index('?')]
    else:
        insee_id = url[url.rindex('/') + 1:]
    return os.path.join(constants.INSEE_DATA_FOLDER, insee_id, file_name)


def split_extractor_input_line(line):
    ''' Return `downloaded_date,url,file,description,published_date` from csv
        input file of the extractor

    Args:
      line (str): a line from csv input file
    '''
    tokens = line.split(',')
    downloaded_date, url, file_name = tokens[0:3]
    description = ' '.join(tokens[3:-1])
    published_date = tokens[-1]
    return downloaded_date, url, get_file_path(url, file_name), \
        description, published_date


if __name__ == '__main__':
    if is_debug_mode():
        DEBUG = True
        logger = get_log('/tmp/tmp.log')
        extract((sys.argv[1], 0))
    else:
        list_args = list()
        extractor_input_file = sys.argv[1]
        with open(extractor_input_file) as f:
            next(f)
            for line in f.read().splitlines():
                # Only extract data from HTML table
                if 'tableau' in line.lower():
                    list_args.append(split_extractor_input_line(line))

        if is_random_test():
            logger = get_log('/tmp/tmp.log')

            num_files = int(sys.argv[2])
            random.seed(2017)
            random.shuffle(list_args)

            extract_html_tables(list_args[:num_files])
        else:
            extract_html_tables(list_args)
