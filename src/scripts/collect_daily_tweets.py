from datetime import datetime
from tweets import tweet_extractor


current_day = datetime.now().strftime('%Y-%m-%d')
tweet_extractor.collect_daily_tweets(current_day)
