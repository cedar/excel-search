# -*- coding: utf-8 -*-

import csv

TABLE_NAME_SEPARATOR = ' – '
TABLE_NAME_PREFIX = 'tableau '
LIST_INSEE_FILES = '../../data/insee_files.csv'
with open(LIST_INSEE_FILES) as f:
    csv_reader = csv.reader(f, quotechar='"', delimiter=',')
    next(csv_reader)
    for row in csv_reader:
        if len(row) > 4:
            data, link, file_name = row[0], row[1], row[2]
            description = ' '.join(row[3:])
        else:
            data, link, file_name, description = row
        file_name = file_name.lower()
        if '.html' in file_name and 'tableau' in file_name and 'graphique' not in file_name:
            if TABLE_NAME_SEPARATOR in description:
                names = description.split(' – ')
                if len(names) > 1:
                    print(names[1])
            else:
                print(description.replace(TABLE_NAME_PREFIX, ''))
