# -*- coding: utf-8 -*-

import codecs
import random
import time
from config import redis_conf
from config.file_path import GEONAMES_FILE

import redis

from utils.log_utils import get_log
from utils.text_utils import normalize_geoname

r_geonames = redis.StrictRedis(host='localhost', port=6379,
                               db=redis_conf.REDIS_DBS['geoname_id'])

set_geonames = set()
dict_geonames = dict()
logger = get_log('../logs/get_french_geonames.log', overwrite=True)


def __write_geonames_to_file():
    with codecs.open(GEONAMES_FILE, 'w+', encoding='utf-8') as f:
        for name in set_geonames:
            f.write(name.strip() + '\n')


def __write_geonames_to_redis():
    for name, geoname_id in dict_geonames.items():
        r_geonames.set(name.lower(), geoname_id)


def extract_geonames():
    '''
    feature classes:
        A: country, state, region,...
        P: city, village,...
    feature codes:
        RGN: region
        RGNE: economic region
    '''
    import csv
    with open('../data/FR.txt') as f:
        reader = csv.reader(f, delimiter='\t')
        for row in reader:
            geoname_id, name, feature_class, feature_code = \
                row[0], row[1], row[6], row[7]
            if feature_class == 'P' or feature_class == 'A' or \
                    feature_code == 'RGN' or feature_code == 'RGNE':
                list_geonames = [normalize_geoname(
                    _name) for _name in name.split(',')]

                set_geonames.update(list_geonames)

                for name in list_geonames:
                    dict_geonames[name] = geoname_id

    __write_geonames_to_file()
    __write_geonames_to_redis()


if __name__ == '__main__':
    extract_geonames()
