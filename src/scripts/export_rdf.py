import subprocess
from utils.async_utils import parallel


def run_command(command):
    output = subprocess.check_output(command, shell=True).decode()
    return output.split('\n')[:-1]


def write_file(f):
    text = open(f).readlines()[21:]
    output_file.writelines(text)
        

output_file = open('/data/tcao/insee-rdf.ttl', 'a+')
files = run_command('find /data/tcao/insee/ -name "*.ttl"')

print('Num of files', len(files))
parallel(write_file, files)
