import os
import sys
import threading
from config.constants import HTML_TTL_FILES, XLS_TTL_FILES
from subprocess import call

from utils import worker_pool
from utils.log_utils import get_log

logger = get_log('../logs/update_rdf_server.log', overwrite=True)
semaphore = threading.Semaphore(36)

''' Load data '''
list_commands = list()
list_ttl_files = list()
with open(HTML_TTL_FILES) as f:
    list_ttl_files.extend(f.read().splitlines())
with open(XLS_TTL_FILES) as f:
    list_ttl_files.extend(f.read().splitlines())

if '--dev' in sys.argv:
    import random
    random.seed(2017)
    random.shuffle(list_ttl_files)
    num_files = int(sys.argv[1])
    list_ttl_files = list_ttl_files[:num_files]
for ttl_file in list_ttl_files:
    list_commands.append(
        's-put http://localhost:3030/insee/data http://insee.xtr{} {}'.format(
            ttl_file, ttl_file
        ))


list_commands = list(set(list_commands))
logger.info('Executing {} commands'.format(len(list_commands)))


def update_rdf_server(command):
    os.system(command)
    logger.info(command)


def run_command(cmd):
    with semaphore:
        os.system(cmd)


def update_using_worker_pool():
    for command in list_commands:
        call(command)
    worker_pool.parallel(update_rdf_server, list_commands, 0.5, 10000, logger)


def update_using_threads():
    for command in list_commands:
        threading.Thread(target=run_command, args=(command, )).start()


def tdbloader_bulk_insert(data_folder):
    ''' Number of files we have in each iteration '''
    batch_size = 500
    if '--dev' in sys.argv:
        batch_size = 10

    num_files = len(list_ttl_files)
    num_iterations = int(num_files / batch_size)

    for iteration in range(num_iterations + 1):
        files = list_ttl_files[iteration * batch_size:
                               min(num_files - 1, (1 + iteration) * batch_size)]
        if files:
            file_args = ' '.join(['"' + ttl_file + '"' for ttl_file in files])
            logger.info('tdbloader --loc {} {}'.format(data_folder, file_args))
            os.system('tdbloader --loc {} {}'.format(data_folder, file_args))


if __name__ == '__main__':
    if '--help' in sys.argv:
        print('update_rdf_server.py num_files tdb_data_folder --dev')
        print('OR')
        print('update_rdf_server.py tdb_data_folder')
    elif '--dev' in sys.argv:
        ''' update_rdf_server.py num_files tdb_data_folder --dev '''
        tdbloader_bulk_insert(sys.argv[2])
    else:
        ''' update_rdf_server.py tdb_data_folder '''
        tdbloader_bulk_insert(sys.argv[1])
