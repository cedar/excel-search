import os
import pickle
import sys
from config.constants import (HTML_TTL_FILES, TABLE_METADATA_PICKLE_FOLDER,
                              TTL_FILE_PLACEHOLDER, XLS_TTL_FILES)

import rdflib
from utils import rdf_utils

from data_models import TableMetadata
from utils import log_utils, worker_pool

logger = log_utils.get_log(
    '../logs/collect_table_metadata.log', overwrite=True)

ATTRIBUTE_PLACE_HOLDER = '##'

DATASET_QUERY = '''
prefix inseeXtr: <http://inseeXtr.excel/>
prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
select ?value
where {
    ?dataset inseeXtr:%s ?value .
    ?dataset rdf:type inseeXtr:Dataset .
    ?sheet inseeXtr:belongs_to ?dataset .
    ?sheet inseeXtr:ttl_file_path "%s" .
}
'''

SHEET_QUERY = '''
prefix inseeXtr: <http://inseeXtr.excel/>
prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
select ?value
where {
    ?sheet inseeXtr:ttl_file_path "%s" .
    ?sheet inseeXtr:%s ?value .
    ?sheet rdf:type inseeXtr:Sheet
}
'''

HEADER_ROW_QUERY = '''
prefix inseeXtr: <http://inseeXtr.excel/>
prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
select ?header ?header_value
where {
    ?sheet inseeXtr:ttl_file_path "%s" .
    ?header inseeXtr:belongs_to ?sheet .
    ?header inseeXtr:value ?header_value .
    ?header rdf:type inseeXtr:HeaderRow .
}
'''

HEADER_COLUMN_QUERY = '''
prefix inseeXtr: <http://inseeXtr.excel/>
prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
select ?header ?header_value
where {
    ?sheet inseeXtr:ttl_file_path "%s" .
    ?header inseeXtr:belongs_to ?sheet .
    ?header inseeXtr:value ?header_value .
    ?header rdf:type inseeXtr:HeaderColumn .
}
'''


def __get_header_id(header):
    '''
    inseeXtr:S_abcdef_HR_1 --> 1
    '''
    return header.split('_')[-1]


def collect_table_metadata(arg):
    '''
    Collect `TableMetadata` from ttl file extracted from Excel file from excel-extractor project
    '''
    ttl_file, index = arg

    table_metadata = TableMetadata()
    table_metadata.file_path = ttl_file

    # for row in graph.query(DATASET_QUERY % 'description'):
    #     table_metadata.description = row.value

    query_results = rdf_utils.query(
        ttl_file, SHEET_QUERY % (ttl_file, 'title'))
    if query_results:
        table_metadata.title = query_results[0]['value']['value']

    query_results = rdf_utils.query(
        ttl_file, SHEET_QUERY % (ttl_file, 'comment'))
    if query_results:
        table_metadata.comment = query_results[0]['value']['value']

    query_results = rdf_utils.query(
        ttl_file,
        DATASET_QUERY % ('url', ttl_file))
    if query_results:
        table_metadata.link = query_results[0]['value']['value']

    for row in rdf_utils.query(ttl_file, HEADER_ROW_QUERY % ttl_file):
        header_value = row['header_value']['value']
        header_id = __get_header_id(row['header']['value'])
        table_metadata.header_rows[header_id] = header_value

    for row in rdf_utils.query(ttl_file, HEADER_COLUMN_QUERY % ttl_file):
        header_value = row['header_value']['value']
        header_id = __get_header_id(row['header']['value'])
        table_metadata.header_columns[header_id] = header_value

    if len(table_metadata.header_rows) == 0:
        logger.error('Error in file ' + ttl_file)
    if len(table_metadata.header_columns) == 0:
        logger.error('Error in file ' + ttl_file)

    pickle.dump(table_metadata, open('%s/%d.pkl' %
                                     (TABLE_METADATA_PICKLE_FOLDER, index), 'wb'))
    logger.info('Collecting table metadata done ' + ttl_file)


def main(dev_mode=False):
    if not os.path.exists(TABLE_METADATA_PICKLE_FOLDER):
        os.makedirs(TABLE_METADATA_PICKLE_FOLDER)

    logger.info('TABLE_METADATA_PICKLE_FOLDER', TABLE_METADATA_PICKLE_FOLDER)

    all_ttl_files = list()
    if '--xls' in sys.argv:
        with open(XLS_TTL_FILES) as f:
            all_ttl_files.extend(f.read().splitlines())
    if '--html' in sys.argv:
        with open(HTML_TTL_FILES) as f:
            all_ttl_files.extend(f.read().splitlines())

    ''' Run on a small sample to verify if everything works correctly '''
    if dev_mode:
        import random
        random.seed(2017)
        random.shuffle(all_ttl_files)
        all_ttl_files = all_ttl_files[:100]

    indices = range(len(all_ttl_files))
    worker_pool.parallel(collect_table_metadata,
                         list(zip(all_ttl_files, indices)), 0.5, 1000, logger)


if __name__ == '__main__':
    main('--dev' in sys.argv)
