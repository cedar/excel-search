# -*- coding: utf-8 -*-

import csv

TABLE_NAME_SEPARATOR = ' – '
TABLE_NAME_PREFIX = 'tableau '
LIST_INSEE_FILES = '../../data/insee_files.csv'
DATA_FOLDER_PREFIX = '/data/tcao/insee_data/'
URL_PREFIX = 'https://www.insee.fr/fr/statistiques/'
with open(LIST_INSEE_FILES) as f:
    csv_reader = csv.reader(f, quotechar='"', delimiter=',')
    next(csv_reader)
    for row in csv_reader:
        if len(row) > 4:
            data, link, file_name = row[0], row[1], row[2]
            description = ' '.join(row[3:])
        else:
            data, link, file_name, description = row
        if '.html' in file_name.lower() and 'tableau' in file_name.lower() and 'graphique' not in file_name.lower():
            if '?' in link:
                link = link[:link.index('?')]
            file_path = DATA_FOLDER_PREFIX + \
                link.replace(URL_PREFIX, '') + '/' + file_name
            print(file_path)
