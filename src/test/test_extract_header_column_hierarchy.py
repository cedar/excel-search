from bs4 import BeautifulSoup

from scripts.extract_html_tables import (extract_header_column_hierarchy,
                                         get_num_header_columns, get_trs)


def __test(html_file, expected_column_hierarchy, expected_data_rows):
    with open(html_file) as f:
        html = f.read()
        soup = BeautifulSoup(html, 'html.parser')

    header_row_trs, header_column_trs = get_trs(soup)

    hiearchy, dict_header_cells, last_column_cell_ids, data_rows = \
        extract_header_column_hierarchy(
            header_column_trs, get_num_header_columns(header_row_trs))

    header_hierarchy = list()
    for child, parent in hiearchy.items():
        header_hierarchy.append(
            (dict_header_cells[child],
             dict_header_cells[parent] if parent is not None else None))

    assert header_hierarchy == expected_column_hierarchy
    assert data_rows == expected_data_rows


def test7():
    # https://www.insee.fr/fr/statistiques/1281293#produit-tableau-tableau1
    __test('test/data/header_hierarchy/7.html',
           [
               ('Chiffre d’affaires (en millions d’euros)', None),
               ('1996', 'Chiffre d’affaires (en millions d’euros)'),
               ('2006', 'Chiffre d’affaires (en millions d’euros)'),
               ('Variation 1996-2006', 'Chiffre d’affaires (en millions d’euros)'),
               ('Effectif salarié  (en équivalent temps plein)', None),
               ('1996', 'Effectif salarié  (en équivalent temps plein)'),
               ('2006', 'Effectif salarié  (en équivalent temps plein)'),
               ('Variation 1996-2006', 'Effectif salarié  (en équivalent temps plein)')
           ],
           [
               ['6 486', '///', '2 840', '9 326', '98 674'],
               ['5 680', '///', '17 297', '162 224'],
               ['5 680', '− 2 840', '7 971', '63 550'],
               ['40 772', '///', '18 516', '59 288', '609 382'],
               ['27 752', '///', '89 023', '820 215'],
               ['27 752', '− 18 516', '29 735', '210 833']
           ])


def test8():
    # https://www.insee.fr/fr/statistiques/1373004#produit-tableau-figure3
    __test('test/data/header_hierarchy/8.html',
           [
               ('Alsace', None),
               ('Champagne-Ardenne', None),
               ('Lorraine', None),
               ('Alsace Champagne-Ardenne Lorraine', None),
           ],
           [
               ['1 868,8', '0,4', '0,5', '-0,1', '763,5', '74,5', '-0,2', '9,2'],
               ['1 338,1', '0,0', '0,3', '-0,3', '519,6', '72,0', '-0,7', '10,9'],
               ['2 346,3', '0,1', '0,2', '-0,1', '824,8', '76,3', '-1,0', '10,6'],
               ['5 553,2', '0,2', '0,3', '-0,1', '2 107,9', '74,6', '-0,6', '10,2']
           ])


def test9():
    # https://www.insee.fr/fr/statistiques/2121815#produit-tableau-Figure0201-1#produit-tableau-Figure0201-1
    __test('test/data/header_hierarchy/9.html',
           [
               ('Bourgogne-Franche-Comté', None),
               ('hors micro-entrepreneurs', 'Bourgogne-Franche-Comté'),
               ('y compris micro-entrepreneurs', 'Bourgogne-Franche-Comté'),
               ('France métropolitaine', None),
               ('hors micro-entrepreneurs', 'France métropolitaine'),
               ('y compris micro-entrepreneurs', 'France métropolitaine')
           ],
           [
               ['2 323', '2 288', '2 358', '3,1', '1,5'],
               ['3 992', '4 017', '3 867', '-3,7', '-3,1'],
               ['79 971', '81 960', '82 343', '0,5', '3,0'],
               ['137 013', '139 819', '137 709', '-1,5', '0,5']
           ]
           )


def test10():
    # https://www.insee.fr/fr/statistiques/2574954#produit-tableau-figure3
    __test('test/data/header_hierarchy/10.html',
           [
               ('Industrie manufacturière', None),
               ('Energies, extraction, eau, déchets', None),
               ('Construction', None),
               ('Commerce', None),
               ('Transport et entreposage', None),
               ('Hébergement/restauration', None),
               ('Information et communication', None),
               ('Finance, assurance', None),
               ('Immobilier', None),
               ('Activités scientifiques/techniques', None),
               ('Services admin. et de soutien', None),
               ('Administration publique', None),
               ('Santé et action sociale', None),
               ('Art et spectacles', None),
               ('Autres services', None),
               ('Ensemble', None)
           ],
           [
               ['1 899', '1 957', '3 037', '1 288'],
               ['1 775', '2 239', '3 486', '976'],
               ['1 338', '1 645', '2 566', '1 277'],
               ['1 532', '858', '3 823', '1 356'],
               ['1 656', '3 670', '1 459', '674'],
               ['739', '785', '1 458', '618'],
               ['2 661', '2 772', '5 334', '1 998'],
               ['2 153', '2 104', '6 703', '1 970'],
               ['1 128', '1 430', '5 052', '1 790'],
               ['2 534', '2 672', '5 816', '3 019'],
               ['1 212', '1 347', '1 908', '1 411'],
               ['2 140', '1 752', '2 628', '2 406'],
               ['2 632', '3 122', '1 706', '1 388'],
               ['1 759', '1 192', '3 780', '2 266'],
               ['1 805', '1 322', '2 665', '1 244'],
               ['1 858', '2 432', '3 342', '1 444']
           ]
           )
