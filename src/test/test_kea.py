# -*- coding: utf-8 -*-

from kea import kea


def test_new_region_names():
    sentence = u"l'espérance de vie dans la région des Hauts-de-France"
    tokens = kea.tokenizer().tokenize(sentence)
    assert tokens == [u"l'", u'esp\xe9rance', u'de', u'vie',
                      u'dans', u'la', u'r\xe9gion', u'des', u'Hauts-de-France']
