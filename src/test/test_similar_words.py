# -*- coding: utf-8 -*-

from utils import text_utils


def test_aides_sociales():
    similar = text_utils.similar_words(u'aides_sociales', 10)
    assert similar[u'aides_sociales'] == 1.0
    assert similar[u'aide_social'] == 1.0
    assert u'allocations_chômage' in similar
    assert u'allocation_chômage' in similar


def test_montants():
    similar = text_utils.similar_words(u'montants', 25)
    assert similar[u'montants'] == 1.0
    assert similar[u'montant'] == 1.0
    assert u'montant_mensuel' in similar


def test_temps():
    similar = text_utils.similar_words(u'temps', 3400)
    assert u'durée' in similar


def test_2016():
    similar = text_utils.similar_words(u'2016', 100)
    assert u'2016' in similar
    assert len(similar) == 1


def test_paris():
    similar = text_utils.similar_words(u'Paris', 100)
    assert u'Paris' in similar
    assert len(similar) == 1


def test_paris2():
    similar = text_utils.similar_words(u'paris', 100)
    assert u'paris' in similar
    assert len(similar) == 1


def test_commnues():
    similar = text_utils.similar_words(u'Communes', 100)
    assert 'île-de-france' not in similar.keys()
