# -*- coding: utf-8 -*-

from utils import text_utils


def test_keep_stop_words():
    assert list(text_utils.tokenize(u"bénéficiaires et montants des aides sociales jeunes en 2014")) == [
        u'b\xe9n\xe9ficiaires', u'et', u'montants', u'des', u'aides_sociales', u'jeunes', u'en', u'2014']


def test_bigram():
    assert list(text_utils.tokenize(u"bénéficiaires et montants des aides sociales jeunes en 2014", remove_stop_words=True)) == [
        u'b\xe9n\xe9ficiaires', u'montants', u'aides_sociales', u'jeunes', u'2014']


def test_l_apostrophe():
    assert list(text_utils.tokenize(
        u"l'espérance de vie dans la région des Hauts-de-France",
        remove_stop_words=True)) == [u'esp\xe9rance', u'vie', u'r\xe9gion', u'Hauts-de-France']


def test_l_apostrophe_lower():
    assert list(text_utils.tokenize(
        u"l'espérance de vie dans la région des Hauts-de-France",
        remove_stop_words=True,
        lower=True)) == [u'esp\xe9rance', u'vie', u'r\xe9gion', u'hauts-de-france']


def test_lower_case():
    assert list(text_utils.tokenize(
        u"l'espérance de vie dans la région des hauts-de-france",
        remove_stop_words=True)) == [u'esp\xe9rance', u'vie', u'r\xe9gion', u'hauts-de-france']


def test_new_province_name():
    assert list(text_utils.tokenize(
        u"Population de la Charente-Maritime",
        remove_stop_words=True, lower=True)) == ['population', 'charente-maritime']


def test_new_province_name2():
    assert list(text_utils.tokenize(
        u"chômage de Alpes-Maritimes",
        remove_stop_words=True, lower=True)) == ['chômage', 'alpes-maritimes']


def test_ngram_name():
    assert list(text_utils.tokenize(u"Charente-Maritime Charente",
                                    remove_stop_words=True,
                                    lower=True)) == ['charente-maritime', 'charente']


def test_ngram_name2():
    assert list(text_utils.tokenize(u"Île-de-France",
                                    remove_stop_words=True,
                                    lower=True)) == [u'île-de-france']


def test_identify_place_name():
    assert list(text_utils.tokenize(
        u"chômage de nord pas de calais",
        remove_stop_words=True, lower=True, identify_place_name=True)) == ['chômage', 'nord-pas-de-calais']


def test_identify_place_name2():
    assert list(text_utils.tokenize(
        u"chômage de Nord Pas de Calais",
        remove_stop_words=True, lower=True, identify_place_name=True)) == ['chômage', 'nord-pas-de-calais']


def test_clean_accents():
    assert list(text_utils.tokenize(
        u"chômage de Île-de-France", remove_stop_words=True, lower=True, 
        identify_place_name=True, need_clean_accents=True)) == ['chomage', 'ile-de-france']


def test_clean_accents2():
    assert list(text_utils.tokenize(
        u"chomage ile-de-france", remove_stop_words=True, lower=True, 
        identify_place_name=True, need_clean_accents=True)) == ['chomage', 'ile-de-france']
