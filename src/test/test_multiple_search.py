import time
from algorithm import document_ranking as dr
from algorithm.document_ranking_params import (HEADER_KEYWORD_COUNTING_MODE,
                                               LIST_G1_FUNCTIONS,
                                               LIST_G2_FUNCTIONS, P_VALUES)


def test():
    now = time.time()
    query = 'salaire net'

    combined_results = dict()
    for g1_name, g1_function in LIST_G1_FUNCTIONS.items():
        for g2_name, g2_function in LIST_G2_FUNCTIONS.items():
            results = dr.search(query, k=20, p=3,
                                g1=g1_function,
                                g2=g2_function,
                                word_score_mode='max',
                                header_keyword_counting_mode='data_cells_or_0')
            for result in results:
                ttl_file = result['ttl_file']
                combined_results[ttl_file] = result

    documents = list(combined_results.values())
    print(len(documents))

    print('Search time = ', time.time() - now)
