from utils import rdf_utils as r

RDF_FILE = 'test.ttl'


def __remove_newlines(html_text):
    return html_text.replace('\n', '').replace('\t', '').replace('  ', '')


def test_published_date():
    r.get_published_date(RDF_FILE) == 'published_date'


def test_header_row_hierarchy():
    assert r.get_header_hierarchy(RDF_FILE) == {
        'HC_0': 'HC_2', 'HC_1': 'HC_2', 'HR_0': 'HR_2', 'HR_1': 'HR_2'}


def test_sheet_uri_name():
    from data_models import Sheet
    assert r.get_sheet_uri_name(RDF_FILE) == 'S_{}_0'.format(Sheet(
        ttl_file_path=RDF_FILE).sheet_id())


def test_get_data_cell():
    assert r.get_data_cell(RDF_FILE, ['HR#2', 'HR#0'], ['HC#2', 'HC#1']) == __remove_newlines('''
    <table border="1" style="border-style: solid; border-collapse: collapse">
      <tr>
        <td rowspan="2" colspan="2"></td>
        <td style="color: red">HR top</td>
      </tr>
      <tr>
        <td style="color: red">HR child 0</td>
      </tr>
      <tr>
        <td style="color: blue">HC top</td>
        <td style="color: blue">HC child 1</td>
        <td>c</td>
      </tr>
    </table>
    ''')


def test_get_data_cell_2():
    assert r.get_data_cell(RDF_FILE, ['HR#0'], ['HC#0']) == __remove_newlines('''
    <table border="1" style="border-style: solid; border-collapse: collapse">
      <tr>
        <td rowspan="1" colspan="1"></td>
        <td style="color: red">HR child 0</td>
      </tr>
      <tr>
        <td style="color: blue">HC child 0</td>
        <td>a</td>
      </tr>
    </table>
    ''')


def test_get_data_cell_3():
    assert r.get_data_cell(RDF_FILE, ['HR#2', 'HR#1'], ['HC#0']) == __remove_newlines('''
    <table border="1" style="border-style: solid; border-collapse: collapse">
      <tr>
        <td rowspan="2" colspan="1"></td>
        <td style="color: red">HR top</td>
      </tr>
      <tr>
        <td style="color: red">HR child 1</td>
      </tr>
      <tr>
        <td style="color: blue">HC child 0</td>
        <td>b</td>
      </tr>
    </table>
    ''')


def test_get_data_cell_empty_column():
    assert r.get_data_cell(RDF_FILE, ['HR#1'], []) == __remove_newlines('''
    <table border="1" style="border-style: solid; border-collapse: collapse">
      <tr>
        <td rowspan="1" colspan="1"></td>
        <td style="color: red">HR child 1</td>
      </tr>
      <tr>
        <td>&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;</td>
      </tr>
    </table>
    ''')


def test_get_data_cell_empty_row():
    assert r.get_data_cell(RDF_FILE, [], ['HC#1']) == __remove_newlines('''
    <table border="1" style="border-style: solid; border-collapse: collapse">
      <tr>
        <td style="color: blue">HC child 1</td>
        <td>&nbsp;&nbsp;</td>
      </tr>
    </table>

    ''')
