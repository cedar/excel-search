from bs4 import BeautifulSoup

from scripts.extract_html_tables import extract_header_row_hierarchy, get_trs


def __test(html_file, expected_tuples):
    with open(html_file) as f:
        html = f.read()
        soup = BeautifulSoup(html, 'html.parser')

    header_row_trs, _ = get_trs(soup)

    hiearchy, dict_header_cells, last_row_ids = extract_header_row_hierarchy(
        header_row_trs)

    header_hierarchy = list()
    for child, parent in hiearchy.items():
        header_hierarchy.append(
            (dict_header_cells[child],
             dict_header_cells[parent] if parent is not None else None))

    assert header_hierarchy == expected_tuples


# https://www.insee.fr/fr/statistiques/1373004#figure4
def test1():
    __test('test/data/header_hierarchy/1.html', [
        ("Part du secteur dans l'emploi salarié (%)", None),
        ('Variation annuelle moyenne (fin 2008 à fin 2013, %)', None),
        ('Région', "Part du secteur dans l'emploi salarié (%)"),
        ('France', "Part du secteur dans l'emploi salarié (%)"),
        ('Région', 'Variation annuelle moyenne (fin 2008 à fin 2013, %)'),
        ('France', 'Variation annuelle moyenne (fin 2008 à fin 2013, %)')
    ])


# https://www.insee.fr/fr/statistiques/1373004#figure3
def test2():
    __test('test/data/header_hierarchy/2.html', [
        ('Population au 01/01/2014p (milliers)', None),
        ("Taux d'évolution annuel moyen de la population 2006-2011 (%)", None),
        ('Emploi au 31/12/2013 au lieu de travail', None),
        ("Variation annuelle moyenne de l'emploi (fin 2008 à fin 2013)", None),
        ('Taux de chômage\n                                             au 3e trim. 2014', None),
        ('Total', "Taux d'évolution annuel moyen de la population 2006-2011 (%)"),
        ('Dû au solde naturel',
         "Taux d'évolution annuel moyen de la population 2006-2011 (%)"),
        ('Dû au solde apparent des entrées-sorties',
         "Taux d'évolution annuel moyen de la population 2006-2011 (%)"),
        ('Total (milliers)', 'Emploi au 31/12/2013 au lieu de travail'),
        ('dont : tertiaire1 (%)', 'Emploi au 31/12/2013 au lieu de travail'),
        ('(%)', "Variation annuelle moyenne de l'emploi (fin 2008 à fin 2013)"),
        ('(%)', 'Taux de chômage\n                                             au 3e trim. 2014'),
    ])


def test3():
    __test('test/data/header_hierarchy/3.html', [
        ('b1', None), ('b2', None), ('b3', None),
        ('c1', 'b2'), ('c2', 'b2'), ('c3', 'b3'), ('c4', 'b3')
    ])


def test4():
    __test('test/data/header_hierarchy/4.html', [
        ('a1', None), ('a2', None),
        ('b1', 'a1'), ('b2', 'a1'),
        ('b3', 'a2'), ('b4', 'a2'),
        ('c1', 'b3'), ('c2', 'b3'), ('c3', 'b4'), ('c4', 'b4')
    ])


def test5():
    __test('test/data/header_hierarchy/5.html', [
        ('t1', None), ('t2', None),
        ('a1', 't1'), ('a2', 't1'),
        ('b1', 'a1'),
        ('b2', 'a2'), ('b3', 'a2'),
        ('b4', 't2'), ('b5', 't2'),
        ('c1', 'b4'), ('c2', 'b4'),
        ('c3', 'b5'), ('c4', 'b5')
    ])


def test6():
    __test('test/data/header_hierarchy/6.html', [
        ('a1', None), ('a2', None),
        ('b3', 'a2'), ('b4', 'a2'),
        ('b1', 'a1'), ('b2', 'a1'),
        ('c1', 'b3'), ('c2', 'b3'), ('c3', 'b4'), ('c4', 'b4')
    ])
