from algorithm.html_table_hierarchy import hierarchy, header_rows_hierarchy


def test_one_parent_header_1():
    assert hierarchy(
        [(0, 1), (1, 1)],
        [(2, 2)]
    ) == {0: 2, 1: 2}


def test_one_parent_header_2():
    assert hierarchy(
        [(0, 1), (1, 1), (2, 1)],
        [(3, 3)]
    ) == {0: 3, 1: 3, 2: 3}


def test_two_parent_headers_1():
    assert hierarchy(
        [(0, 1), (1, 1), (2, 1), (3, 1), (4, 1)],
        [(5, 3),                 (6, 2)]
    ) == {0: 5, 1: 5, 2: 5,       3: 6, 4: 6}


def test_two_parent_headers_2():
    assert hierarchy(
        [(0, 1), (1, 1), (2, 1), (3, 1)],
        [(5, 3),                 (6, 1)]
    ) == {0: 5, 1: 5, 2: 5,       3: 6}


def test_two_parent_headers_3():
    assert hierarchy(
        [(0, 1), (1, 1), (2, 1), (3, 1), (4, 1)],
        [(5, 3),                 (6, 1), (7, 1)]
    ) == {0: 5, 1: 5, 2: 5,       3: 6,  4: 7}


def test_two_parent_headers_3():
    assert hierarchy(
        [(5, 3), (6, 2), (7, 1), (8, 1)],
        [(9, 5),         (10, 2)]
    ) == {5: 9, 6: 9, 7: 10, 8: 10}


def test_two_parent_headers_4():
    assert hierarchy(
        [(5, 3), (6, 2), (7, 2), (8, 1)],
        [(9, 5),         (10, 3)]
    ) == {5: 9, 6: 9, 7: 10, 8: 10}


def test_two_parent_headers_5():
    assert hierarchy(
        [(5, 3), (6, 2), (7, 1), (8, 2)],
        [(9, 5),         (10, 3)]
    ) == {5: 9, 6: 9, 7: 10, 8: 10}


def test_duplicated_header_name():
    assert hierarchy(
        [(1, 2), (3, 1), (4, 1)],
        [(1, 2), (2, 2)]
    ) == {3: 2, 4: 2}


def test_two_header_rows():
    assert header_rows_hierarchy([
        [(2, 2)],
        [(0, 1), (1, 1)]
    ]) == {0: 2, 1: 2, 2: None}


def test_two_header_rows_rowspan():
    assert header_rows_hierarchy([
        [(2, 2), (3, 2)],
        [(2, 2), (0, 1), (1, 1)]
    ]) == {2: None, 3: None, 0: 3, 1: 3}


def test_three_header_rows():
    assert header_rows_hierarchy([
        [(11, 8)],
        [(9, 5),         (10, 3)],
        [(5, 3), (6, 2), (7, 1), (8, 2)]
    ]) == {5: 9, 6: 9, 7: 10, 8: 10, 9: 11, 10: 11, 11: None}


def test_three_header_rows_rowspan():
    assert header_rows_hierarchy([
        [(2, 3), (3, 4)],
        [(2, 3), (4, 2),         (5, 2)],
        [(2, 3), (0, 1), (1, 1), (7, 1), (8, 1)]
    ]) == {2: None, 0: 4, 1: 4, 7: 5, 8: 5, 4: 3, 5: 3, 3: None}
