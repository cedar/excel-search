import os

from utils import rdf_utils
from data_models import DataSet, Sheet
from utils.rdf_converter import RdfConverter

RDF_FILE = 'test.ttl'
TEST_TDB_DATA_FOLDER = '~/tmp'
if os.path.exists(TEST_TDB_DATA_FOLDER):
    os.makedirs(TEST_TDB_DATA_FOLDER)


rdf_converter = RdfConverter(RDF_FILE,
                             DataSet('url', 'downloaded_date',
                                     'downloaded_file_path', 'description'),
                             Sheet(0, 'title', 'comment',
                                   'test.ttl', 'published_date'))
rdf_converter.write_header_rows(
    {0: 2, 1: 2, 2: None},
    {0: 'HR child 0', 1: 'HR child 1', 2: 'HR top'},
)

rdf_converter.write_header_columns(
    {0: 2, 1: 2, 2: None},
    {0: 'HC child 0', 1: 'HC child 1', 2: 'HC top'},
)

rdf_converter.write_data_rows(
    [
        ['a', 'b'],
        ['c', 'd']
    ],
    [0, 1],
    [0, 1]
)

os.system('kill $(lsof -t -i:3030)')
os.system('rm -rf {}'.format(TEST_TDB_DATA_FOLDER))
os.system('mkdir {}'.format(TEST_TDB_DATA_FOLDER))
os.system('$JENAROOT/bin/tdbloader --loc %s %s' %
          (TEST_TDB_DATA_FOLDER, RDF_FILE))
os.system('rm {}'.format(RDF_FILE))
os.system('cd $FUSEKI &&' +
          './fuseki-server --update --loc %s /insee &' % TEST_TDB_DATA_FOLDER)
