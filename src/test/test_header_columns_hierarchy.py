from algorithm.html_table_hierarchy import hierarchy, header_columns_hierarchy


def test_one_parent_header_1():
    assert header_columns_hierarchy([
        [0, 1]
    ]) == {0: None, 1: 0}


def test_one_parent_header_2():
    assert header_columns_hierarchy([
        [0, 1, 2]
    ]) == {0: None, 1: 0, 2: 1}


def test_two_parent_headers_1():
    assert header_columns_hierarchy([
        [0, 1],
        [2, 3]
    ]) == {0: None, 1: 0, 2: None, 3: 2}


def test_two_parent_headers():
    assert header_columns_hierarchy([
        [0, 1],
        [2, 3]
    ]) == {0: None, 1: 0, 2: None, 3: 2}


def test_parent_headers_rowspan_1():
    assert header_columns_hierarchy([
        [0, 1],
        [0, 3]
    ]) == {0: None, 1: 0, 3: 0}


def test_parent_headers_rowspan_2():
    assert header_columns_hierarchy([
        [0, 1, 2],
        [0, 1, 3],
        [0, 4, 5],
    ]) == {0: None, 1: 0, 2: 1, 3: 1, 4: 0, 5: 4}
