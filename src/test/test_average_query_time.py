import time
from evaluation import list_queries_in_dev_set, list_queries_in_test_set
from algorithm import document_ranking as dr
from algorithm.document_ranking_params import (HEADER_KEYWORD_COUNTING_MODE,
                                               LIST_G1_FUNCTIONS,
                                               LIST_G2_FUNCTIONS)

EVALUATION_FOLDER = '../evaluation/dev_29_queries'
DEV_SET_QUERIES = EVALUATION_FOLDER + '/queries.txt'


def test():
    import logging
    logging.getLogger('TreeTagger').setLevel(logging.WARNING)

    queries = list_queries_in_dev_set(1)
    queries.extend(list_queries_in_test_set())
    assert len(queries) == 55

    now = time.time()
    for query in queries:
        search_results = dr.search(query, k=20, p=3,
                                   g1=LIST_G1_FUNCTIONS['g1.1'],
                                   g2=LIST_G2_FUNCTIONS['g2.5.dev'],
                                   word_score_mode='max',
                                   header_keyword_counting_mode='data_cells_or_0')
    print('Average query time', (time.time() - now) / len(queries))
