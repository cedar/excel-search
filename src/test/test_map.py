import pytest

from utils.average_precision import apk, mapk, mapk2


def test1():
    assert mapk(actual=[['a']], predicted=[['b', 'a', 'c', 'd']], k=3) == 0.5
    assert mapk2(actual=[['a', 'x']], predicted=[
                ['b', 'a', 'c', 'd']], k=3) == 0.5
    assert mapk(actual=[['a']], predicted=[
                ['b', 'e', 'a', 'c', 'd']], k=3) == pytest.approx(1.0 / 3)


def test2():
    assert mapk(actual=[['e'], ['a']], predicted=[
                ['a', 'b', 'c'], ['b', 'a', 'c', 'd']], k=3) == 0.25
