from algorithm.fagin import fagin
import algorithm.document_ranking as dr


def test_1():
    list_dictionaries = [
        {'a': 10, 'b': 9, 'c': 4, 'd': 3},
        {'b': 10, 'c': 9, 'd': 8, 'a': 4}
    ]

    assert fagin(2, list_dictionaries, sum) == [('b', 19), ('a', 14)]


def test_2():
    list_dictionaries = [
        {'d1': .13, 'd3': .13, 'd6': .08, 'd5': .07},
        {'d2': .24, 'd1': .2, 'd5': .1}
    ]

    assert fagin(3, list_dictionaries, sum) == [
        ('d1', 0.33), ('d2', 0.24), ('d5', 0.17)]


def test_3():
    list_dictionaries = [
        {'doc': 1.0},
        {'doc': 1.0},
        {'doc': 1.0},
    ]

    assert fagin(1, list_dictionaries, sum) == [('doc', 3.0)]


def test_4():
    list_dictionaries = [
        {'doc': 1.0},
        dict(),
        {'doc': 1.0},
    ]

    assert fagin(1, list_dictionaries, sum) == [('doc', 2.0)]


def test_5():
    list_dictionaries = [
        {'doc': 1.0},
        {'doc_2': 1.0},
        {'doc': 1.0},
    ]

    assert fagin(1, list_dictionaries, sum) == [('doc', 2.0)]


def test_6():
    list_dictionaries = [
        {'a': 1.0, 'b': 1.0, 'c': 1.0, 'd': 0.5},
        {'a': 1.0, 'b': 1.0, 'c': 0.9},
        {'a': 1.0, 'A': 1.0, 'c': 0.5},
    ]

    assert fagin(2, list_dictionaries, sum) == [('a', 3.0), ('c', 2.4)]


def test_intersection():
    '''
    Ensure that top-k results should be included in top-n results where n < k
    '''
    import random
    random.seed(2017)

    num_lists = 10
    num_docs = 200
    docs = ['doc_' + str(i) for i in range(num_docs)]

    list_dictionaries = list()
    for i in range(num_lists):
        dictionary = dict()
        for j in range(random.randint(0, num_docs)):
            dictionary[random.choice(docs)] = random.random()
        list_dictionaries.append(dictionary)

    list_2 = fagin(2, list_dictionaries, sum)
    list_5 = fagin(5, list_dictionaries, sum)
    list_10 = fagin(10, list_dictionaries, sum)
    assert set(list_2).intersection(list_5) == set(list_2)
    assert set(list_5).intersection(list_10) == set(list_5)


def test_g1():
    assert dr.g1([0]) == 1
    assert dr.g1([0, 0]) == 1
    assert dr.g1([0, 1]) == 11
    assert dr.g1([1, 2]) == 103


def test_g2():
    assert dr.g2((1, 1, 1)) == 12
