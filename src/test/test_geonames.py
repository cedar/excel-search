from utils.geonames_utils import get_hierarchy


def test():
    assert get_hierarchy('montpellier') == [
        'montpellier', 'hérault', 'occitanie']


def test2():
    assert get_hierarchy('not a geoname') == []
