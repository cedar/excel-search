from utils import text_utils


def test_remove_filtered_words():
    assert text_utils.remove_filtered_words(['one'], 'one')[0] == ''


def test_remove_filtered_words2():
    assert text_utils.remove_filtered_words(['one'], 'one two')[0] == 'two'
    assert text_utils.remove_filtered_words(['one'], 'two one')[0] == 'two'


def test_remove_filtered_words3():
    assert text_utils.remove_filtered_words(['one'], 'two')[0] == 'two'


def test_tokenize_place_name():
    assert text_utils.tokenize_place_name('a-b') == ['a', 'b']
    assert text_utils.tokenize_place_name('a/b') == ['a', 'b']
    assert text_utils.tokenize_place_name('a b') == ['a', 'b']
    assert text_utils.tokenize_place_name('a-b c') == ['a', 'b', 'c']
    assert text_utils.tokenize_place_name('a-b/c') == ['a', 'b', 'c']


def test_filter_place_names():
    list_place_names = ['nord-pas-de-calais']
    text_utils.remove_filtered_words(list_place_names, 
            'nord pas de calais')[1] == 'nord-pas-de-calais'
    text_utils.remove_filtered_words(list_place_names, 
            'nord pas-de calais')[1] == 'nord-pas-de-calais'


def test_filter_place_names2():
    assert text_utils.filter_out_place_name('nord pas de calais')[1] == 'nord-pas-de-calais'
    assert text_utils.filter_out_place_name('Nord Pas de Calais')[1] == 'nord-pas-de-calais'
    assert text_utils.filter_out_place_name('nord pas-de-calais')[1] == 'nord-pas-de-calais'


def test_filter_place_names3():
    assert text_utils.filter_out_place_name('chômage nord pas de calais') == \
        ('chômage', 'nord-pas-de-calais')

