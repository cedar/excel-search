from algorithm.data_cell_search2 import the_most_relevant_data_cell


def test():
    results = the_most_relevant_data_cell(
        [
            {'HR#0': 1.0},
            {'HC#0': 1.0}
        ],
        'test.ttl'
    )

    for hr, hc, dc in results:
        assert hr == 'HR child 0'
        assert hc == 'HC child 0'
        assert dc == 'a'


def test2():
    results = the_most_relevant_data_cell(
        [
            {'HR#0': 1.0},
            {}
        ],
        'test.ttl'
    )

    for hr, hc, dc in results:
        assert hr == 'HR child 0'
        assert hc == ''
        assert dc == ''


def test3():
    results = the_most_relevant_data_cell(
        [
            {'HC#0': 1.0},
        ],
        'test.ttl'
    )

    for hr, hc, dc in results:
        assert hr == ''
        assert hc == 'HC child 0'
        assert dc == ''


def test4():
    results = the_most_relevant_data_cell(
        [
            {},
        ],
        'test.ttl'
    )

    for hr, hc, dc in results:
        assert hr == ''
        assert hc == ''
        assert dc == ''
