# -*- coding: utf-8 -*-

from utils import text_utils


def _test_proper_name(name):
    assert text_utils.word_to_lemma(name) == name
    assert text_utils._is_geoname(name)


def test_proper_name():
    _test_proper_name(u'Hauts-de-Seine')


def test_proper_name2():
    _test_proper_name(u'Île-de-France')


def test_proper_name3():
    _test_proper_name(u'Charente-Maritime')


def test_number():
    assert text_utils.word_to_lemma(u'2016') == u'2016'
