import os


''' INSEE data folder '''
INSEE_DATA_FOLDER = '/data/tcao/insee/'

''' List of the generated .ttl files from scripts/extract_html_tables.py '''
HTML_TTL_FILES = '/data/tcao/html_ttl_files.txt'

''' List of the generated .ttl files from project excel-extractor '''
XLS_TTL_FILES = '/data/tcao/xls_ttl_files.txt'

''' Store `TableMetadata` pickle files generated from scripts/collect_table_metadata.py'''
TABLE_METADATA_PICKLE_FOLDER = '/data/tcao/table-metadata-pickle/'
if os.environ.get('TABLE_METADATA_PICKLE_FOLDER'):
    TABLE_METADATA_PICKLE_FOLDER = os.environ.get(
        'TABLE_METADATA_PICKLE_FOLDER')

''' RDF server where we store all generated RDF files from extractors '''
RDF_SERVER = 'http://localhost:3030/insee'

''' Placeholder for ttl file path '''
TTL_FILE_PLACEHOLDER = '#ttl_file#'
