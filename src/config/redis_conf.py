

def get_word_document_score_key(sim, lower, version):
    if version == 1:
        return 'word_document_score_sim_{}_lower_{}'.format(sim, lower)
    else:
        return 'word_document_score_sim_{}_lower_{}_v{}'.format(sim, lower, version)


def get_word_document_location_key(sim, lower, version):
    if version == 1:
        return 'word_document_location_sim_{}_lower_{}'.format(sim, lower)
    else:
        return 'word_document_location_sim_{}_lower_{}_v{}'.format(sim, lower, version)


REDIS_DBS = {
    # PRODUCTION
    'evaluation_data': 0,
    'users': 1,
    # 'table_titles': 2,
    # 'document_file_path': 3,
    'geoname_id': 4,

    get_word_document_score_key(100, True, 5): 12,
    get_word_document_location_key(100, True, 5): 13,

    # DEV
    'table_titles': 5,
    'links': 6,
    'ttl_file_paths': 7,
    get_word_document_score_key(50, True, 6): 14,
    get_word_document_location_key(50, True, 6): 15,
}

REDIS_COLLECTIONS_KEY_LOCATION_SCORE = 'location_score'
REDIS_COLLECTIONS_KEY_DOCUMENT_SCORE = 'document_score'
