if [ "$1"  = "--help" ]; then
  echo './document_score.sh --dev'
  echo 'OR ./document_score.sh'
elif [ "$1"  = "--dev" ]; then
  # 100 random files
  rm -rf /home/cedar/tcao/tmp/
  mkdir /home/cedar/tcao/tmp/
  export TABLE_METADATA_PICKLE_FOLDER='/home/cedar/tcao/tmp/'
fi
python -m scripts.collect_tablemetadata --html --xls
python word_document_score.py
