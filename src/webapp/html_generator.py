

def __is_empty_cell(cell):
    return cell is None or cell.strip() == ''


def data_cell(header_row, header_column, data_cell):
    if __is_empty_cell(header_row) and __is_empty_cell(header_column):
        return 'Pas de cellule identifiée, regardez le tableau!'
    else:
        row_html = '<tr><td rowspan="1" colspan="1"></td><td style="color: red">{}</td></tr>'.format(
            header_row)
        column_html = '<td style="color: blue">{}</td>'.format(header_column)

        return '<table border="1" style="border-style: solid; border-collapse: collapse">{}{}</table>'.format(
            row_html,
            '<tr>{}<td>{}</td></tr>'.format(column_html, data_cell)
        )
