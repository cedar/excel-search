# -*- coding: utf-8 -*-
import datetime
import os
import hashlib
import json
import logging
import math
import sys
import time
from config import redis_conf
from urllib.parse import quote_plus, unquote

import redis
from flask import (Flask, flash, g, jsonify, redirect, render_template,
                   request, url_for, Response)
from werkzeug.security import check_password_hash, generate_password_hash

from tweets import mention_extractor, tweet_extractor
import algorithm.data_cell_search2 as dcs
from algorithm import document_ranking as dr
from algorithm.document_ranking_params import (HEADER_KEYWORD_COUNTING_MODE,
                                               LIST_G1_FUNCTIONS,
                                               LIST_G2_FUNCTIONS)
from flask_login import (LoginManager, current_user, login_required,
                         login_user, logout_user)
from utils.rdf_utils import get_data_cell, get_published_date
from webapp import html_generator
from word_document_score import link


# def global_score_function(arg):
#     t, hr, hc, c = arg
#     return math.log(1 + t) + 0.75 * (math.log(1 + hr) + math.log(1 + hc)) + 0.5 * math.log(1 + hc) * math.log(1 + hr) + 0.5 * math.log(1 + c)
def global_score_function(arg):
    t, h, g1 = arg
    return t + 2 * h + g1


LOG_FILE = 'dev.log' if '--dev' in sys.argv else 'insee-search.log'
NO_EVALUATION = '-1'
PRODUCTION_PORT = 8080
DEVELOPMENT_PORT = 6000

# Flask app configuration
app = Flask(__name__)
app.config.from_pyfile('insee_search.cfg')

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_message = ''
login_manager.login_view = 'login'


handler = logging.FileHandler(LOG_FILE)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
app.logger.addHandler(handler)
logger = app.logger
if '--dev' in sys.argv:
    logger.setLevel(level=logging.DEBUG)
else:
    logger.setLevel(level=logging.INFO)

# Redis
r = redis.StrictRedis(host='localhost', port=6379,
                      db=redis_conf.REDIS_DBS['evaluation_data'])
r_users = redis.StrictRedis(host='localhost', port=6379,
                            db=redis_conf.REDIS_DBS['users'])
r_table_titles = redis.StrictRedis(host='localhost', port=6379,
                                   db=redis_conf.REDIS_DBS['table_titles'])
r_ttl_file_paths = redis.StrictRedis(host='localhost', port=6379,
                                     db=redis_conf.REDIS_DBS['ttl_file_paths'])


# Other variables
query_cache = dict()
''' Store the result of query '''


class User(object):

    def __init__(self, username, password_hash):
        self.username = username
        self.password_hash = password_hash

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.username

    def __repr__(self):
        return '<User %r>' % (self.username)


def __get_hm_value(r, key, field):
    result = r.hmget(key, [field])
    if result:
        if result[0] is not None:
            return result[0].decode('utf-8')
    return None


def __table_row_data(_id, d, hr, hc, s, query):
    ttl_file = r_ttl_file_paths.get(d).decode('utf-8')

    # list_small_html_tables = list()
    # for header_row_hierarchy in hr:
    #     for header_column_hierarchy in hc:
    #         list_small_html_tables.append(get_data_cell(
    #             ttl_file, header_row_hierarchy, header_column_hierarchy))

    result = {'id': str(_id + 1),
              '_query_': query.replace(' ', '%20'),
              'link': link(d),
              'title': r_table_titles.get(link(d)).decode('utf-8'),
              'ttl': ttl_file,
              'published_date': get_published_date(ttl_file),
              #   'data_cell': "<br/>".join(list_small_html_tables),
              'data_cell': "",
              'score': s}
    return result


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('register.html')
    user_info = request.form.to_dict()
    # Check if this user existed
    existing_user = __get_hm_value(r_users, user_info['username'], 'username')
    if existing_user is not None:
        flash(u"Nom d'utilisateur invalide", 'error')
        return redirect(url_for('register'))
    # Save password's hash
    user_info['password'] = generate_password_hash(user_info['password'])
    r_users.hmset(request.form['username'], user_info)
    flash(u"L'utilisateur est enregistré")
    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    username = request.form['username']
    password = request.form['password']
    remember_me = False
    if 'remember_me' in request.form:
        remember_me = True

    password_hash = __get_hm_value(r_users, username, 'password')
    if password_hash is None:
        flash(u"Nom d'utilisateur invalide", 'error')
        return redirect(url_for('login'))
    if not check_password_hash(password_hash, password):
        flash('Mot de passe invalide', 'error')
        return redirect(url_for('login'))

    login_user(User(username, password_hash), remember=remember_me)
    return redirect(url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@login_manager.user_loader
def load_user(username):
    password_hash = __get_hm_value(r_users, username, 'password')
    if password_hash is None:
        return None
    return User(username, password_hash)


@app.before_request
def before_request():
    g.user = current_user


@app.route("/")
@login_required
def index():
    return render_template('index.html')


def __table_row_data2(_id, d, s, query, global_scores):
    ttl_file = r_ttl_file_paths.get(d).decode('utf-8')

    data_row = '''
    <tr {bg_color}>
       <td>{id}</td>
       <td>
         {title}
         <br/>
         <a href={link}>{link}</a>
         <br/>
         {ttl}
         <br/>
         {global_scores}
       </td>
       <td>{published_date} &nbsp;&nbsp;&nbsp</td>
       <td>{score} &nbsp;&nbsp;&nbsp</td>
       <td><div id="data_cell_{id}">Chargement...</div></td>
       <td>
          <form name="_form_{id}">
            <input type="radio" name="_evaluation_{id}" value="-1" checked="checked"> rien</input>
            <input type="radio" name="_evaluation_{id}" value="0"> pas pertinent</input>
            <input type="radio" name="_evaluation_{id}" value="1"> un peu pertinent</input>
            <input type="radio" name="_evaluation_{id}" value="2"> bien pertinent</input>
            <br/>
            <input type="text" name="_comment_{id}" placeholder="Commentaire" size="50"/>
            <input type="hidden" name="_ttl_{id}" value="{ttl_quote}" />
            <input type="hidden" name="_link_{id}" value="{link}" />
            <input type="hidden" name="_query_" value="{_query_}" />
          </form>
      </td>
    </tr>
    '''

    has_been_evaluated = False
    evaluation_code = ''
    for evaluation_id in r.keys():
        evaluation = {k.decode('utf-8'): v.decode('utf-8') for k, v
                      in r.hgetall(evaluation_id.decode('utf-8')).items()}

        has_been_evaluated = current_user.username == evaluation[
            'username'] and query == evaluation['query'] and ttl_file == evaluation['ttl']
        if has_been_evaluated:
            if evaluation['evaluation'] == '2':
                evaluation_code = '#99cc99'
            elif evaluation['evaluation'] == '1':
                evaluation_code = '#ffffe5'
            elif evaluation['evaluation'] == '0':
                evaluation_code = '#ff9999'
            break

    result = {'id': str(_id + 1),
              '_query_': quote_plus(query),
              'link': link(d),
              'title': r_table_titles.get(link(d)).decode('utf-8'),
              'ttl': ttl_file,
              'ttl_quote': quote_plus(ttl_file),
              'published_date': get_published_date(ttl_file),
              'score': "{0:.4f}".format(s),
              'global_scores': global_scores,
              'bg_color': 'bgcolor="{}"'.format(evaluation_code) if has_been_evaluated else ''}
    return data_row.format(**result)


@app.route("/result_table/<query>")
@login_required
def get_result_table(query):
    app.logger.info('Query: ' + query)
    now = time.time()

    # combined_results = dict()
    # for g1_name, g1_function in LIST_G1_FUNCTIONS.items():
    #     for g2_name, g2_function in LIST_G2_FUNCTIONS.items():
    #         for header_keyword_counting_mode in HEADER_KEYWORD_COUNTING_MODE:
    #             results = dr.search(query, k=20, p=3,
    #                                 g1=g1_function,
    #                                 g2=g2_function,
    #                                 word_score_mode='max',
    #                                 header_keyword_counting_mode=header_keyword_counting_mode)
    #             for result in results:
    #                 ttl_file = result['ttl_file']
    #                 combined_results[ttl_file] = result
    # search_results = list(combined_results.values())

    search_results = dr.search(query, k=10, p=3,
                               g1=LIST_G1_FUNCTIONS['g1.1'],
                               g2=LIST_G2_FUNCTIONS['g2.5.dev'],
                               word_score_mode='max',
                               header_keyword_counting_mode='data_cells_or_0')

    app.logger.info(
        'Document ranking finished in {} seconds'.format(time.time() - now))
    json_data = [__table_row_data2(_id,
                                   result['document_id'],
                                   result['score'],
                                   query,
                                   str(result['global_scores']))
                 for _id, result in enumerate(search_results)]
    table_header = '''
    <tr>
        <td><b>Rang</b></td>
        <td><b>Lien</b></td>
        <td><b>Date de publication</b></td>
        <td><b>Score</b></td>
        <td><b>Cellule de donnée</b></td>
        <td><b>Votre évaluation</b></td>
    </tr>
    '''
    data_cell_inputs = [{
        'ttl_file': result['ttl_file'],
        'list_location_score_dict': result['list_location_score_dict']}
        for result in search_results]
    return jsonify({'table': json_data, 'data_cell_inputs': data_cell_inputs, 'table_header': table_header})


@app.route("/data_cell")
@login_required
def get_data_cell():
    ttl_file = request.args.get('ttl_file')
    list_location_score_dict = request.args.get('list_location_score_dict')
    row_id = request.args.get('row_id')

    data_cell_tuples = [html_generator.data_cell(hr, hc, dc) + '<br/>' for hr, hc, dc in dcs.the_most_relevant_data_cell(
        json.loads(list_location_score_dict), ttl_file)]

    if len(data_cell_tuples) > 1:
        first_result = data_cell_tuples[0] + \
            ' <input type="button" id="button_more_%s" value="Tous les résultats">' % row_id
    elif len(data_cell_tuples) == 1:
        first_result = data_cell_tuples[0]
    else:
        first_result = ''

    return jsonify({'row_id': row_id, 'first_result': first_result, 'cells': data_cell_tuples})


@app.route("/save", methods=['POST'])
def save_evaluation():
    query = request.form['_query_']
    app.logger.info('Saving evaluation for query: ' + query)
    for key in request.form.keys():
        if '_evaluation_' in key:
            item_id = key.replace('_evaluation_', '')
            evaluation = request.form[key]
            ttl = request.form['_ttl_' + item_id]
            app.logger.info('\tSave {} {}'.format(link, evaluation))

            evaluation_id = hashlib.sha224(
                (ttl + query + current_user.username).encode('utf-8')).hexdigest()
            r.hmset(str(evaluation_id), {
                'username': current_user.username,
                'query': unquote(query).replace('+', ' '),
                'evaluation': evaluation,
                'comment': request.form['_comment_' + item_id],
                'ttl': unquote(ttl),
                'link': request.form['_link_' + item_id],
                'time': f"{datetime.datetime.now():%Y-%m-%d %H:%M:%S}"
            })
    return jsonify({'response': 'OK'})


@app.route("/evaluations")
@login_required
def evaluation_results():
    evaluations = list()
    for evaluation_id in r.keys():
        evaluation = r.hgetall(evaluation_id.decode('utf-8'))
        evaluations.append(
            {k.decode('utf-8'): v.decode('utf-8') for k, v in evaluation.items()})
    return render_template('evaluations.html', items=evaluations)


def stream_template(template_name, **context):
    app.update_template_context(context)
    t = app.jinja_env.get_template(template_name)
    rv = t.stream(context)
    # rv.enable_buffering(5)
    return rv


@app.route("/daily_tweets")
def daily_tweets():
    return render_template('daily_tweets.html', \
            all_folders=os.listdir('/data/tcao/daily_tweets_politique1'))


@app.route('/tweets')
def collect_tweets():
    return Response(stream_template('tweets_collector.html', \
            tweets=tweet_extractor.collect_tweets(since=request.args.get('since'),
                until=request.args.get('until'))))

            
@app.route('/mentions')
def tweets_of_mention():
    return Response(stream_template('mentions.html', \
            tweets=mention_extractor.extract_from_candidate(since=request.args.get('since'),
                until=request.args.get('until'))))


@app.route('/mentions2/<day>')
def tweets_of_mention2(day):
    return Response(stream_template('mentions.html', \
            tweets=mention_extractor.extract_from_candidate2(published_date=day)))


if __name__ == "__main__":
    dev_mode = '--dev' in sys.argv
    if dev_mode:
        app.run(port=DEVELOPMENT_PORT if dev_mode else PRODUCTION_PORT, processes=20)
    else:
        app.run(host='0.0.0.0', port=DEVELOPMENT_PORT if dev_mode else PRODUCTION_PORT, processes=20)
