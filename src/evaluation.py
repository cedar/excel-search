import os
import sys
import pickle
from config import redis_conf

import redis

from algorithm import document_ranking as dr
from algorithm.document_ranking_params import (HEADER_KEYWORD_COUNTING_MODE,
                                               LIST_G1_FUNCTIONS,
                                               LIST_G2_FUNCTIONS, P_VALUES)
from utils.average_precision import mapk, mapk2, apk

EVALUATION_FOLDER = '../evaluation/dev_29_queries'
DEV_SET_QUERIES = EVALUATION_FOLDER + '/queries.txt'
MAP_SCORES_DEV_MIN_1 = EVALUATION_FOLDER + '/map_scores_dev_min_1.txt'
MAP_SCORES_DEV_MIN_2 = EVALUATION_FOLDER + '/map_scores_dev_min_2.txt'


def get_dev_set(queries=[], min_evaluation_score=1):
    dict_query_documents = get_evaluation_data(min_evaluation_score, 'cao')

    gold_standard_documents = list()
    for query in queries:
        gold_standard_documents.append(dict_query_documents[query])

    return gold_standard_documents


def get_evaluation_data(min_evaluation_score, username):
    r = redis.StrictRedis(host='localhost', port=6379,
                          db=redis_conf.REDIS_DBS['evaluation_data'])

    # skipped_queries = [
            # '''taux de chômage du mois de mai 2013 pour la France la France 2013''',
            # '''taux de emploi France Allemagne 2003''',
            # '''PIB par habitant   Allemagne France 1999''',
            # '''taux de chômage le plus faible Allemagne 2012''',
            # '''PIB par habitant France 1999''',
            # '''PIB Portugal Bruxelles 2013''',
            # '''taux de emploi s vacants dans le UE28 2015''',
            # '''nombre de demandeurs de emploi sans activité à la fin du mois de juillet La France 2014''',
            # '''niveau de vie de les Français Français Hexagone 2014''',
            # '''taux de chômage en France métropolitaine France 2012''',
    # ]
    queries = '''taux de chômage en France métropolitaine 2012
niveau de vie moyen de les plus de 75 ans 2013
niveau de vie moyen de les 20 % de personnes les plus modestes 2013
PIB par habitant 2007
taux de emploi Etat la France Suède 2012
taux de emploi en CDD ou intérim 2016
consommation de les ménages France 2015
niveau de vie médian 2014
nombre de demandeurs de emploi en activité réduite 2009
PIB Espagne 2013
consommation de les ménages en 2011 2012
taux de emploi France 2003
taux de chômage en données corrigées de les variations saisonnières 2013
taux de chômage France 2001
inflation de les prix 2011
taux de chômage élevé de la population active dans la zone euro 2015
nombre de demandeurs de emploi sans aucune activité ( catégorie A ) 2015
SMIC horaire 2009
inflation moyenne de en France en 2016
taux de chômage 2014
consommation de les ménages 2015
PIB 2014
inflation 2012
SMIC 2012
inflation à Lille  2013''' .split('\n')
    queries = '''taux de chômage de les immigrés issus de les pays tiers à le Union européenne Français 2012
taux de chômage en France métropolitaine 2012
niveau de vie moyen de les plus de 75 ans 2013
niveau de vie moyen de les 20 % de personnes les plus modestes 2013
PIB par habitant 2007
taux de emploi Etat la France Suède 2012
taux de emploi en CDD ou intérim 2016
consommation de les ménages France 2015
niveau de vie médian 2014
nombre de demandeurs de emploi en activité réduite 2009
PIB Espagne 2013
consommation de les ménages en 2011 2012
taux de emploi France 2003
taux de chômage en données corrigées de les variations saisonnières 2013
taux de chômage France 2001
inflation de les prix 2011
taux de chômage élevé de la population active dans la zone euro 2015
nombre de demandeurs de emploi sans aucune activité ( catégorie A ) 2015
consommation de les ménages France 2015
SMIC horaire 2009
inflation moyenne de en France en 2016
inflation à Lille  2013
taux de chômage 2014
consommation de les ménages 2015
PIB 2014
inflation 2012
SMIC 2012'''.split('\n')

    dict_query_documents = dict()
    for evaluation_id in r.keys():
        data_row = {k.decode('utf-8'): v.decode('utf-8') for k, v
                    in r.hgetall(evaluation_id.decode('utf-8')).items()}
        query = data_row['query'].strip()
        if query not in queries:
            continue
        if query == "création d'entreprisesdepuis 2002":
            query = "création d'entreprises depuis 2002"
        if query == "créations d'entrepriseen 2016":
            query = "créations d'entreprise en 2016"

        document = data_row['ttl']
        if data_row['username'] == username:
            if query not in dict_query_documents:
                dict_query_documents[query] = list()
            if int(data_row['evaluation']) >= min_evaluation_score:
                dict_query_documents[query].append(document)

    return dict_query_documents


def rank_documents(queries,
                   g1,
                   g2,
                   header_keyword_counting_mode,
                   k,
                   contains_mandatory_keywords):
    predicted = list()
    for query in queries:
        # print(query)
        predicted.append(
            [result['ttl_file']
                for result in dr.search(query, k, 3,
                                        g1, g2, 'max', header_keyword_counting_mode, contains_mandatory_keywords)]
        )
    return predicted


def _write_map_score(g1_name, g1_function, g2_name, g2_function,
        header_keyword_counting_mode):
    queries = open(DEV_SET_QUERIES).read().splitlines() 
    predicted = rank_documents(queries,
                   g1=g1_function,
                   g2=g2_function, 
                   header_keyword_counting_mode=header_keyword_counting_mode)
    
    actual = get_dev_set(queries, min_evaluation_score=1)
    map_score = mapk2(actual, predicted, k=20)
    with open(MAP_SCORES_DEV_MIN_1, 'a+') as f:
        f.write(
            f'{g1_name, g2_name, header_keyword_counting_mode, map_score}\n')

    actual = get_dev_set(queries, min_evaluation_score=2)
    map_score = mapk2(actual, predicted, k=20)
    with open(MAP_SCORES_DEV_MIN_2, 'a+') as f:
        f.write(
            f'{g1_name, g2_name, header_keyword_counting_mode, map_score}\n')

def parameters_search():
    for header_keyword_counting_mode in HEADER_KEYWORD_COUNTING_MODE:
        for g1_name, g1_function in LIST_G1_FUNCTIONS.items():
            for g2_name, g2_function in LIST_G2_FUNCTIONS.items():
                _write_map_score(g1_name,
                        g1_function,
                        g2_name,
                        g2_function,
                        header_keyword_counting_mode)


def list_queries_in_dev_set(min_evaluation_score):
    return open(DEV_SET_QUERIES).read().splitlines()


def list_queries_in_test_set():
    return get_evaluation_data(1, 'test').keys()


def best_params_from_map_file(file_name):
    dict_params_scores = dict()
    with open(file_name) as f:
        for line in f.read().splitlines():
            items = line.replace('(', '').replace(')', '').split(',')
            map_score = float(items[-1])
            params = ','.join(items[:-1])
            dict_params_scores[params] = map_score

    best_score = max(dict_params_scores.values())
    print('Dev_set scores {}, best = {}'.format(file_name, best_score))
    for params, map_score in dict_params_scores.items():
        if map_score == best_score:
            print(params)
    print('---')


def evaluate_test_set(min_evaluation_score, username, k=20, show_queries=False, contains_mandatory_keywords=False):
    dict_query_documents = get_evaluation_data(
        min_evaluation_score, username)
    print('Evaluating {} queries'.format(len(dict_query_documents)))
    queries = list(dict_query_documents.keys())
    actual = list(dict_query_documents.values())
    # if show_queries:
        # for query in queries:
            # print(query)

    map_score = mapk2(actual,
                      rank_documents(queries,
                                     g1=LIST_G1_FUNCTIONS['g1.1'],
                                     g2=LIST_G2_FUNCTIONS['g2.5.dev'],
                                     header_keyword_counting_mode='data_cells_or_0',
                                     k=k,
                                     contains_mandatory_keywords=contains_mandatory_keywords),
                      k=k)
    print('Test set MAP@{} {}, min_evaluation_score {}, contains_mandatory_keywords {}'.format(\
            k, map_score, min_evaluation_score, contains_mandatory_keywords))


def evaluate_search_engines():
    insee_csv = '../data/insee_evaluation.txt'
    __search_engine_evaluation(insee_csv, 1, True)
    __search_engine_evaluation(insee_csv, 1, False)
    __search_engine_evaluation(insee_csv, 2, True)
    __search_engine_evaluation(insee_csv, 1, False)

    google_csv = '../evaluation/google_evaluation.txt'
    __search_engine_evaluation(google_csv, 1, False)
    __search_engine_evaluation(google_csv, 2, False)


def __search_engine_evaluation(csv_file, min_evaluation_score, include_pdf):
    with open(csv_file) as f:
        lines = f.read().splitlines()[1:]
        dict_query_preds = dict()
        dict_query_actual = dict()
        for line in lines:
            query_id, document, evaluation = line.split(',')[:3]

            if query_id not in dict_query_preds:
                dict_query_preds[query_id] = list()
            if query_id not in dict_query_actual:
                dict_query_actual[query_id] = list()

            if int(evaluation) >= min_evaluation_score:
                if include_pdf or (not include_pdf and len(line.split(',')) == 3):
                    dict_query_actual[query_id].append(document)
            dict_query_preds[query_id].append(document)

        queries = list(dict_query_preds.keys())
        preds, actual = list(), list()
        for query in queries:
            preds.append(dict_query_preds[query])
            actual.append(dict_query_actual[query])

        print('Evaluation of {} min_evaluation_score = {}, include_pdf = {}'.format(
            csv_file, min_evaluation_score, include_pdf))
        print(mapk2(actual, preds, k=20))


if __name__ == '__main__':
    # Disable TreeTagger logs
    import logging
    logging.getLogger('TreeTagger').setLevel(logging.WARNING)

    evaluate_test_set(1, 'eval', 10)
    # evaluate_test_set(1, 'eval', 10, contains_mandatory_keywords=True)
    # evaluate_test_set(2, 'eval', 10, contains_mandatory_keywords=True)
    evaluate_test_set(2, 'eval', 10)

    if '--dev-set' in sys.argv:
        list_queries_in_dev_set(min_evaluation_score=1)
    
    if '--params-search' in sys.argv:
        parameters_search()

    if '--best' in sys.argv:
        best_params_from_map_file(MAP_SCORES_DEV_MIN_1)
        best_params_from_map_file(MAP_SCORES_DEV_MIN_2)

    if '--score' in sys.argv:
        evaluate_test_set(1, 'test')
        evaluate_test_set(2, 'test')

    if '--search-engines' in sys.argv:
        evaluate_search_engines()
