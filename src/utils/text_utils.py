# -*- coding: utf-8 -*-
import itertools
import re
import os
import platform
from config import constants
from config.file_path import KEA_VILLES_LIST
from functools import lru_cache
import unicodedata
import nltk
import treetaggerwrapper as tw
from gensim.models import Phrases, Word2Vec
from gensim.models.phrases import Phraser
from nltk.corpus import stopwords
from treetaggerwrapper import TreeTaggerError

from kea import kea

if 'oak' in platform.node():
    # Oak cluster
    tagger = tw.TreeTagger(TAGLANG='fr', TAGDIR='/oak-data/tcao/treetagger/')
elif 'cedar' in platform.node() or 'bellini' in platform.node():
    # Cedar cluster
    tagger = tw.TreeTagger(
        TAGLANG='fr', TAGDIR='/home/cedar/tcao/tree-tagger/')
else:
    tagger = tw.TreeTagger(TAGLANG='fr')

with open(KEA_VILLES_LIST) as f:
    all_geonames = {geoname: True for geoname in (
        name.lower() for name in f.read().splitlines())}

GEONAMES_PREFIXES = [
    u"Département de l'",
    u'Département de la',
    u'Département des',
    u'Département de',
    u"Département d'",
    u'Département du',

    u"Arrondissement de l'",
    u'Arrondissement de la',
    u'Arrondissement des',
    u'Arrondissement de',
    u"Arrondissement d'",
    u'Arrondissement du',
]


POS_WITH_NO_SIMILAR_WORDS = ['NUM', 'NAM']

ADDITIONAL_STOP_WORDS = ['les', "l'", "d'", 'lors', 'moyen', "entre"]
stop_words = stopwords.words('french')
stop_words.extend(ADDITIONAL_STOP_WORDS)

kea_tokenizer = kea.tokenizer()

# word2vec and phraser
lang = 'fr'
mincount = 50
size = 300
ngram_level = 2
threshold = 2

assert ngram_level in [0, 1, 2]

if 'cedar' in platform.node() or 'bellini' in platform.node():
    # Cedar cluster
    WORD2VEC_FOLDER = '/data/tcao'
elif 'oak' in platform.node():
    # Oak cluster
    WORD2VEC_FOLDER = '/oak-data/tcao'
else:
    # Local
    WORD2VEC_FOLDER = '/Users/tienduccao/Work/Inria/projects/word2vec_models'

if ngram_level > 0:
    phraser_file = WORD2VEC_FOLDER + '/phraser/phraser_web{}_mincount{}_t{}_level{{}}.bin'.format(
        lang, mincount, threshold)
word2vec_model_file = WORD2VEC_FOLDER + '/word2vec/word2vec_web{}_mincount{}_size{}_phrases.bin'.format(
    lang, mincount, size)

model = Word2Vec.load(word2vec_model_file)
if ngram_level > 0:
    bigram_phraser = Phraser.load(phraser_file.format(1))
    # if ngram_level == 1:
    #     iterator = bigram_phraser[tokenized_sentences]
    # else:
    #     trigram_phraser = Phraser.load(phraser_file.format(2))
    #     iterator = trigram_phraser[bigram_phraser[tokenized_sentences]]


text_to_tags_cache = dict()
word_tag_cache = dict()


def normalize_geoname(name):
    for prefix in GEONAMES_PREFIXES:
        name = name.replace(prefix, '')
    return name.strip()


def _is_geoname(word):
    return word.lower() in all_geonames


def clean_accents(text):
    return unicodedata.normalize('NFD', text).encode('ascii', 'ignore').decode()


def tag_text(text, need_clean_accents=False):
    if need_clean_accents:
        text = clean_accents(text)

    if text and text not in text_to_tags_cache:
        # If the tagger takes too much time to finish, just stop it
        try:
            list_tags = tagger.tag_text(text)

            if list_tags is not None:
                for tag in tw.make_tags(list_tags):
                    if tag is not None and type(tag) == tw.Tag:
                        word_tag_cache[tag.word] = tag
                        text_to_tags_cache[text] = True
        except TreeTaggerError:
            pass


def extract_lemma(tag):
    if tag is not None and type(tag) == tw.Tag:
        if tag.pos == 'NUM':
            return tag.word, tag.pos
        else:
            return tag.lemma, tag.pos
    return '', None


def tag(word):
    if word not in word_tag_cache:
        # If the tagger takes too much time to finish, just stop it
        try:
            list_tags = tagger.tag_text(word)
            if list_tags is not None and len(list_tags) > 0:
                word_tag_cache[word] = tw.make_tags(list_tags)[0]
            else:
                word_tag_cache[word] = None
        except TreeTaggerError:
            word_tag_cache[word] = None
    return word_tag_cache[word]


def word_to_lemma(word):
    if _is_geoname(word):
        return word

    tag_result = tag(word)
    if tag_result:
        lemma, _ = extract_lemma(tag_result)
        return lemma
    return ''


def tokenize(text, remove_stop_words=False, lower=False, need_clean_accents=False,
        identify_place_name=False):
    def _word(word, need_lower, need_clean_accents):
        if need_clean_accents:
            word = clean_accents(word)
        if need_lower:
            word = word.lower()
        return word

    place_name = None
    if identify_place_name:
        normal_text, place_name = filter_out_place_name(text)
        text = normal_text

    if remove_stop_words:
        word_generator = (_word(word, lower, need_clean_accents) 
                for word in bigram_phraser[kea_tokenizer.tokenize(text)] 
                if word.lower() not in stop_words)
    else:
        word_generator = (_word(word, lower, need_clean_accents) 
                for word in bigram_phraser[kea_tokenizer.tokenize(text)])

    return itertools.chain(word_generator, [_word(place_name, lower, need_clean_accents)] if place_name else [])

def phrase_to_lemma(word):
    single_words = word.split('_')
    return '_'.join([word_to_lemma(w) for w in single_words])


def is_number(s):
    try:
        float(s)  # for int, long and float
    except ValueError:
        return False

    return True


def __similar_words(keyword, num_of_similar_words):
    results = dict()
    if keyword in model:
        _, pos = extract_lemma(tag(keyword))
        if pos not in POS_WITH_NO_SIMILAR_WORDS and not is_number(keyword) and not _is_geoname(keyword):
            similar = model.wv.most_similar(
                positive=[keyword], negative=[], topn=num_of_similar_words)
            for (word, score) in similar:
                if not _is_geoname(word):
                    results[word] = score
                # If this word is a phrase
                if '_' in word:
                    lemma_form = phrase_to_lemma(word)
                    if lemma_form != word and not _is_geoname(lemma_form):
                        results[lemma_form] = score
    return results


def similar_words(keyword, num_of_similar_words):
    results = __similar_words(keyword, num_of_similar_words)
    results[keyword] = 1.0

    if '_' in keyword:
        lemma_form = phrase_to_lemma(keyword)
    else:
        lemma_form = word_to_lemma(keyword)

    if lemma_form != keyword and lemma_form != '':
        results[lemma_form] = 1.0
        results.update(__similar_words(lemma_form, num_of_similar_words))
    return results


def clean_space_characters(text):
    return ' '.join(text.split()).replace('\t', ' ').replace('\n', ' ')


def build_all_names_regex(list_filtered_words):
    list_regex = build_list_filtered_place_names(list_filtered_words)
    return re.compile('|'.join([word_regex for word_regex in list_regex]))


def remove_filtered_words(list_filtered_words, text):
    '''
    If any word (phrase) from `list_filtered_words` appears in `text` then 
    it will be removed from `text`.
    The matched text (if it exists) will also be returned.
    '''
    regex_compile = build_all_names_regex(list_filtered_words) 
    return filter_text(regex_compile, list_filtered_words, text)


def filter_text(regex_compile, list_filtered_words, text):
    '''
    If a substring of `text` is matched with `regex_compile` then remove 
    that match from `text`.
    The matched substring is also returned.
    '''
    matcher = regex_compile.search(text)
    if matcher is not None:
        matched_text = re.search(place_name_regex(matcher.group()), 
                ' '.join(list_filtered_words)).group()
    else:
        matched_text = None
    
    return re.sub(regex_compile, '', text).strip(), matched_text

def tokenize_place_name(place_name):
    return re.split('-| |/', place_name)


def place_name_regex(place_name):
    return '\\b' + '[/| |-]+'.join(tokenize_place_name(place_name)) + '\\b'


def build_list_filtered_place_names(list_place_names):
    return [place_name_regex(place_name) for place_name in list_place_names]


with open('kea/resources/villes.list') as f:
    list_original_place_names = [name.lower() for name in f.read().splitlines()]
    list_place_names_no_accents = [clean_accents(name) for name in list_original_place_names]
    list_place_names = list_original_place_names + list_place_names_no_accents

regex_all_place_names = build_all_names_regex(list_place_names)

def filter_out_place_name(text):
    return filter_text(regex_all_place_names, list_place_names, text)
