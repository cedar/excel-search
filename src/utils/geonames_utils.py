from config import redis_conf

import redis
import requests

from utils.text_utils import normalize_geoname

r_geonames = redis.StrictRedis(host='localhost', port=6379,
                               db=redis_conf.REDIS_DBS['geoname_id'])


GEONAMES_HIERARCHY_URL = 'http://api.geonames.org/hierarchy?geonameId={}&username=inseesearch&type=json&lang=fr'


def get_hierarchy(geoname):
    geoname_id = r_geonames.get(geoname.lower())
    if geoname_id:
        geoname_id = geoname_id.decode('utf-8')
        r = requests.get(GEONAMES_HIERARCHY_URL.format(geoname_id))
        if r.status_code == 200:
            json_response = r.json()
            if 'geonames' in json_response:
                # Skip 3 names: Monde -> Europe -> France
                return [normalize_geoname(item['name']).lower() for item in
                        reversed(json_response['geonames'][3:-1])
                        if item['name'] is not None and item['name'] != geoname]

    return list()
