import multiprocessing

def parallel(function, arguments):
    max_processes = multiprocessing.cpu_count() - 2
    pool = multiprocessing.Pool(processes=max_processes)
    results = pool.map(function, arguments)
    pool.close()
    pool.join()
    return results
