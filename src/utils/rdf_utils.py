import json
import subprocess
import time
from config.constants import RDF_SERVER, TTL_FILE_PLACEHOLDER
from urllib.parse import quote_plus

import requests

from data_models import Sheet

EMPTY_CELL = '&nbsp;&nbsp;'


HEADER_X_HIERARCHY_QUERY = '''
PREFIX inseeXtr: <http://inseeXtr.excel/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?child_header ?parent_header
WHERE {
    # ?child_header inseeXtr:belongs_to inseeXtr:%s .
    # ?parent_header inseeXtr:belongs_to inseeXtr:%s .
    ?child_header rdf:type inseeXtr:HeaderColumn .
    ?parent_header rdf:type inseeXtr:HeaderColumn .
    ?child_header inseeXtr:XHierarchy+ ?parent_header .
    VALUES ?child_header {%s} .
    VALUES ?parent_header {%s}
}
'''

HEADER_X_HIERARCHY_QUERY2 = '''
PREFIX inseeXtr: <http://inseeXtr.excel/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT *
WHERE {
    %s
}
'''


HEADER_Y_HIERARCHY_QUERY = '''
PREFIX inseeXtr: <http://inseeXtr.excel/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?child_header ?parent_header
WHERE {
    # ?child_header inseeXtr:belongs_to inseeXtr:%s .
    # ?parent_header inseeXtr:belongs_to inseeXtr:%s .
    ?child_header rdf:type inseeXtr:HeaderRow .
    ?parent_header rdf:type inseeXtr:HeaderRow .
    ?child_header inseeXtr:YHierarchy+ ?parent_header .
    VALUES ?child_header {%s} .
    VALUES ?parent_header {%s}
}
'''

SHEET_QUERY = '''
PREFIX inseeXtr: <http://inseeXtr.excel/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?sheet
WHERE {
    ?sheet rdf:type inseeXtr:Sheet .
    ?sheet inseeXtr:ttl_file_path "%s".
}
'''

HEADER_ROW_VALUE_QUERY = '''
PREFIX inseeXtr: <http://inseeXtr.excel/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?value
WHERE {
    ?header rdf:type inseeXtr:HeaderRow .
    FILTER (?header = inseeXtr:%s_HR_%s) .
    ?header inseeXtr:value ?value .
}
'''

HEADER_VALUES_QUERY = '''
PREFIX inseeXtr: <http://inseeXtr.excel/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?header_value
WHERE {
    VALUES ?header {%s} .
    ?header inseeXtr:value ?header_value .
}
'''

HEADER_COLUMN_VALUE_QUERY = '''
PREFIX inseeXtr: <http://inseeXtr.excel/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?value
WHERE {
    ?header rdf:type inseeXtr:HeaderColumn .
    FILTER (?header = inseeXtr:%s_HC_%s) .
    ?header inseeXtr:value ?value .
}
'''

PUBLISHED_DATE_QUERY = '''
PREFIX inseeXtr: <http://inseeXtr.excel/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?published_date
WHERE {
    ?sheet inseeXtr:ttl_file_path "%s" .
    ?sheet inseeXtr:published_date ?published_date .
}
'''

DATA_CELL_VALUE_QUERY = '''
PREFIX inseeXtr: <http://inseeXtr.excel/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?value
WHERE {
    ?data_cell rdf:type inseeXtr:DataCell .
    ?data_cell inseeXtr:belongs_to inseeXtr:%s .
    ?data_cell inseeXtr:closestXCell ?header_column .
    ?header_column inseeXtr:XHierarchy+ inseeXtr:%s .
    ?data_cell inseeXtr:closestYCell ?header_row .
    ?header_row inseeXtr:YHierarchy+ inseeXtr:%s .
    ?data_cell inseeXtr:value ?value .
}
'''


DATA_CELL_VALUE_QUERY2 = '''
PREFIX inseeXtr: <http://inseeXtr.excel/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?header_row_value ?header_column_value ?data_cell_value
WHERE {
  VALUES ?hr {%s} .
  VALUES ?hc {%s} .

  ?header_row inseeXtr:YHierarchy* ?hr .
  ?header_column inseeXtr:XHierarchy* ?hc .
  ?data_cell inseeXtr:closestYCell ?header_row .
  ?data_cell inseeXtr:closestXCell ?header_column .

  ?data_cell inseeXtr:value ?data_cell_value .
  ?hc inseeXtr:value ?header_column_value .
  ?hr inseeXtr:value ?header_row_value .
}
'''


def __full_uri_to_id(uri):
    '''
    Args:
      uri (str): a full URI, e.g: http://inseeXtr.excel/S_41f75878c49aee1835391bbecb2993741614f659981f0159dac858e1_0_HC_6

    Returns:
      str: the ID, e.g: HC_6
    '''
    return '_'.join(uri.split('_')[-2:])


def __header_hierarchy(sparql_query):
    header_hierarchy = dict()
    for row in rdf_query(sparql_query, return_first_item=False):
        child_header = row['child_header']['value']
        parent_header = row['parent_header']['value']
        header_hierarchy[__full_uri_to_id(
            child_header)] = __full_uri_to_id(parent_header)
    return header_hierarchy


def get_header_hierarchy2(ttl_file, list_header_row_ids, header_type):
    # TODO this is just a quick fix, it should be fixed by re-generating RDF file for HTML with the long URI for object of predicate closestX|Y
    if header_type == 'column':
        hierarchy_type = 'XHierarchy'
    elif header_type == 'row':
        hierarchy_type = 'YHierarchy'
    else:
        assert('Wrong header_type, must be either "row" or "column"')

    sheet_uri_name = get_sheet_uri_name(ttl_file)
    prefix = sheet_uri_name + '_'
    queries = list()

    counter = 0

    selected_ids = set()
    ''' each item is {'uris', 'hiearchy_ids'} '''
    list_hierarchy_info = list()
    for header_row_ids in list_header_row_ids:
        uris = ['inseeXtr:' + prefix + h for h in header_row_ids]
        if len(uris) > 1:
            hierarchy_info = {'uris': uris, 'hierarchy_ids': []}
            # TODO also need to check other permutations of uris
            for i, uri in enumerate(uris):
                if i + 1 <= len(uris) - 1:
                    hierarchy_id = 'hierarchy' + str(counter)

                    queries.append(
                        'BIND( EXISTS {%s inseeXtr:%s+ inseeXtr:%s} as ?%s) .' % (
                            uris[i], hierarchy_type, uris[i + 1], hierarchy_id
                        )
                    )

                    hierarchy_info['hierarchy_ids'].append(hierarchy_id)

                    counter += 1

            if hierarchy_info['hierarchy_ids']:
                list_hierarchy_info.append(hierarchy_info)

        elif len(uris) == 1:
            selected_ids.add(uris[0])

    now = time.time()
    results = rdf_query(HEADER_X_HIERARCHY_QUERY2 %
                        ('\n'.join(queries)), return_first_item=False)
    print('{} seconds for querying {} hierarchies from {}'.format(
        time.time() - now, len(queries), ttl_file))
    hierarchies = [results[0]['hierarchy' +
                              str(_id)]['value'] for _id in range(counter)]
    num_hierarchies = sum((1 for hierarchy in hierarchies if hierarchy))
    print('Num hierarchies found {}'.format(num_hierarchies))

    print('Num selected_ids without hierarchy', len(selected_ids))
    query_json = results[0]
    for hierarchy_info in list_hierarchy_info:
        hierarchy_found = True
        for hierarchy_id in hierarchy_info['hierarchy_ids']:
            if not query_json[hierarchy_id]['value']:
                hierarchy_found = False
                break

        if hierarchy_found:
            selected_ids.update(hierarchy_info['uris'])

    return selected_ids


def get_data_cell2(header_row_ids, header_column_ids):
    if header_row_ids != '' and header_column_ids == '':
        results = rdf_query(HEADER_VALUES_QUERY %
                            header_row_ids, return_first_item=False)
        return ((row['header_value']['value'], '', '') for row in results)
    elif header_column_ids != '' and header_row_ids == '':
        results = rdf_query(HEADER_VALUES_QUERY %
                            header_column_ids, return_first_item=False)
        return (('', row['header_value']['value'], '') for row in results)
    elif header_row_ids == '' and header_column_ids == '':
        return [('', '', '')]
    else:
        query_results = rdf_query(DATA_CELL_VALUE_QUERY2 %
                                  (header_row_ids, header_column_ids), return_first_item=False)
        return ((row['header_row_value']['value'],
                 row['header_column_value']['value'],
                 row['data_cell_value']['value']) for row in query_results)


def get_header_hierarchy(ttl_file, header_row_ids1, header_row_ids2, header_column_ids1, header_column_ids2):
    sheet_uri_name = get_sheet_uri_name(ttl_file)

    # TODO this is just a quick fix, it should be fixed by re-generating RDF file for HTML with the long URI for object of predicate closestX|Y
    if 'multiple' in ttl_file:
        prefix = sheet_uri_name + '_'
    else:
        prefix = ''

    header_hierarchy = dict()
    header_hierarchy.update(__header_hierarchy(
        HEADER_X_HIERARCHY_QUERY % (sheet_uri_name, sheet_uri_name,
                                    ' '.join(['inseeXtr:' + prefix +
                                              hr.replace('#', '_') for hr in header_column_ids1]),
                                    ' '.join(['inseeXtr:' + prefix +
                                              hr.replace('#', '_') for hr in header_column_ids2]))))
    header_hierarchy.update(__header_hierarchy(
        HEADER_Y_HIERARCHY_QUERY % (sheet_uri_name, sheet_uri_name,
                                    ' '.join(['inseeXtr:' + prefix +
                                              hc.replace('#', '_') for hc in header_row_ids2]),
                                    ' '.join(['inseeXtr:' + prefix +
                                              hc.replace('#', '_') for hc in header_row_ids2]))))

    return header_hierarchy


def get_published_date(ttl_file):
    result = rdf_query(PUBLISHED_DATE_QUERY % ttl_file)
    if result is not None and 'published_date' in result:
        return result['published_date']['value']
    else:
        return ''


def get_sheet_uri_name(ttl_file):
    sheet_uri = None
    sheet_uri = rdf_query(SHEET_QUERY % ttl_file)['sheet']['value']

    assert sheet_uri != None
    return sheet_uri.split('/')[-1]


def get_header_rows_html(header_row_hierarchy, num_columns, sheet_uri_name):
    tr_content = ''
    for position, header_row in enumerate(header_row_hierarchy):
        header_row_id = header_row.split('#')[1]
        result = rdf_query(HEADER_ROW_VALUE_QUERY %
                           (sheet_uri_name, header_row_id))
        header_row_value = result['value']['value']

        tr_content += '<tr>'
        if position == 0:
            tr_content += '<td rowspan="{}" colspan="{}"></td>'.format(
                len(header_row_hierarchy), num_columns)
        tr_content += '<td style="color: red">{}</td></tr>'.format(
            header_row_value)
    return tr_content


def get_header_columns_and_data_html(header_column_hierarchy,
                                     data_cell_value, sheet_uri_name):
    tr_content = ''
    for header_column in header_column_hierarchy:
        header_column_id = header_column.split('#')[1]
        result = rdf_query(HEADER_COLUMN_VALUE_QUERY %
                           (sheet_uri_name, header_column_id))
        header_column_value = result['value']['value']

        tr_content += '<td style="color: blue">{}</td>'.format(
            header_column_value)
    if len(header_column_hierarchy) == 0:
        tr_content = '<td>{}</td>'.format(EMPTY_CELL)
    return '<tr>{}<td>{}</td></tr>'.format(tr_content, data_cell_value)


def get_data_cell(ttl_file, header_row_hierarchy, header_column_hierarchy):
    '''
    Args:
        ttl_file (str): path to the .ttl file
        header_row_hierarchy (list of str):
            each item has the form `HR#n`, `n` is the ID of header row in `ttl_file`
            the left item is parent header of the right one
        header_column_hierarchy (list of str):
            each item has the form `HC#n`, `n` is the ID of header column in `ttl_file`
            the left item is parent header of the right one
    Returns:
        HTML table which contains header row(s), header column(s) and data cell
    '''
    sheet_uri_name = get_sheet_uri_name(ttl_file)

    # The two headers which are closest to data cell
    data_cell = EMPTY_CELL
    if len(header_row_hierarchy) > 0 and len(header_column_hierarchy) > 0:
        last_header_row_cell = header_row_hierarchy[-1]
        last_header_column_cell = header_column_hierarchy[-1]
        if '#' in last_header_row_cell and '#' in last_header_column_cell:
            header_row_id = last_header_row_cell.replace('#', '_')
            header_column_id = last_header_column_cell.replace('#', '_')

            # TODO this is just a quick fix, it should be fixed by re-generating RDF file for HTML with the long URI for object of predicate closestX|Y
            if 'multiple' in ttl_file:
                header_row_id = sheet_uri_name + '_' + header_row_id
                header_column_id = sheet_uri_name + '_' + header_column_id
            result = rdf_query(DATA_CELL_VALUE_QUERY % (
                sheet_uri_name, header_column_id, header_row_id))
            if result:
                data_cell = result['value']['value']

    rows_html = get_header_rows_html(
        header_row_hierarchy,
        max(len(header_column_hierarchy), 1),
        sheet_uri_name)
    columns_and_data_html = get_header_columns_and_data_html(
        header_column_hierarchy, data_cell, sheet_uri_name)

    return '<table border="1" style="border-style: solid; border-collapse: collapse">{}{}</table>'.format(rows_html, columns_and_data_html)


def query(ttl_file, query):
    '''
    Args:
      ttl_file (str): path of the ttl file
      query (str): SPARQL query which contains query_item_1, ..., query_item_n

    Returns:
      a list of query's result, each item of this list has the
        following format
        {
            query_item_1: {"type": type_1, "value": value_1},
            ...
            query_item_n: {"type": type_n, "value": value_n},
        }
    '''
    results = requests.get(
        RDF_SERVER + '/query?query=' + quote_plus(query)).json()
    return results['results']['bindings']


def rdf_query(query, return_first_item=True):
    '''
    Args:
      query (str): SPARQL query which contains query_item_1, ..., query_item_n

    Returns:
      a list of query's result if `return_first_item` is True,
          each item of this list has the following format
        {
            query_item_1: {"type": type_1, "value": value_1},
            ...
            query_item_n: {"type": type_n, "value": value_n},
        }
        otherwise return only the first item of result list
    '''
    results = requests.get(
        RDF_SERVER + '/query?query=' + quote_plus(query)).json()
    list_results = results['results']['bindings']

    if return_first_item:
        if list_results:
            return list_results[0]
        else:
            return None
    else:
        return list_results


if __name__ == '__main__':
    # print(get_parent_header('../data/sample_table.ttl',
    #                         [('9', 'HR'), ('1', 'HC')]))
    # print(get_data_cell('../data/sample_table.ttl', 'HR#9', 'HC#22'))
    print(query(
        '/data/tcao/insee/2014360/multiple_sine2006_r30/12/0.ttl',
        '''
        prefix inseeXtr: <http://inseeXtr.excel/>
        prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        select ?header_value where {
            graph <http://insee.xtr{}>
            {
              ?header inseeXtr:value ?header_value .
              ?header rdf:type inseeXtr:HeaderRow
            }
        }'''.format(TTL_FILE_PLACEHOLDER)))
