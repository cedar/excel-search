

def get_attribute(soup_element, attribute_name, default_value):
    if attribute_name in soup_element.attrs:
        return soup_element.attrs[attribute_name]
    return default_value
