import codecs

from data_models import DataSet, Sheet
from utils.text_utils import clean_space_characters

PREFIX = 'inseeXtr'
"""
Prefix of generated RDF file
"""
NAMESPACE_URI = 'http://inseeXtr.excel/'
"""
Namespace of generated RDF file
"""


def uri(id_prefix, _id):
    return "%s:%s_%s" % (PREFIX, id_prefix, _id)


def type_uri(_type):
    return "%s:%s" % (PREFIX, _type)


def predicate(name):
    return "%s:%s" % (PREFIX, name)


def get_uri_prefix(uri):
    return uri.split(':')[1]


def normalized_text(text):
    """
    Clean the double quotes from `text` (`str`).
    Also remove the new line characters.
    """
    return clean_space_characters(text.replace('"', ' '))


class RdfConverter():
    """
    Generate RDF file from extracted `Table` instance
    """

    def __init__(self, output_file, dataset, sheet):
        """
        Args:
            output_file (str): the file path where the generated RDF file will be stored
            dataset (`DataSet`):
            sheet (`Sheet`)
        """
        self.output_file = output_file
        self.f = codecs.open(output_file, 'w+', encoding='utf-8')
        self.file_uri = self.write_file(dataset)
        self.sheet_uri = self.write_sheet(sheet)

    def write_header_cell(self, f, header_uri, cell_value, hierarchy_type,
                          aggregated_cell_uri):
        f.write('%s %s %s ;\n' % (
            header_uri,
            predicate('value'),
            '"%s"' % normalized_text(cell_value)
        ))
        f.write('\t\t%s %s ;\n' % (
            predicate('belongs_to'),
            self.sheet_uri
        ))
        header_type = type_uri('HeaderRow' if hierarchy_type ==
                               'YHierarchy' else 'HeaderColumn')
        if aggregated_cell_uri:
            f.write('\t\t%s %s ;\n' % (
                'rdf:type',
                header_type
            ))
            f.write('\t\t%s %s .\n\n' % (
                predicate(hierarchy_type),
                aggregated_cell_uri
            ))
        else:
            f.write('\t\t%s %s .\n\n' % (
                'rdf:type',
                header_type
            ))

    def write_sheet(self, sheet):
        sheet_uri = uri('S', sheet.sheet_id() + '_' + str(sheet._id))
        self.f.write('%s %s %s ;\n' % (
            sheet_uri,
            predicate('title'),
            '"%s"' % normalized_text(sheet.title)
        ))
        self.f.write('\t\t%s %s ;\n' % (
            'rdf:type',
            type_uri('Sheet')
        ))
        self.f.write('\t\t%s %s ;\n' % (
            predicate('comment'),
            '"%s"' % normalized_text(sheet.comment)
        ))
        self.f.write('\t\t%s %s ;\n' % (
            predicate('ttl_file_path'),
            '"%s"' % sheet.ttl_file_path
        ))
        self.f.write('\t\t%s %s ;\n' % (
            predicate('published_date'),
            '"%s"' % sheet.published_date
        ))
        self.f.write('\t\t%s %s .\n\n' % (
            predicate('belongs_to'),
            self.file_uri
        ))

        return sheet_uri

    def write_file(self, dataset):
        self.f.write("@prefix %s: <%s> .\n" % (PREFIX, NAMESPACE_URI))
        self.f.write(
            "@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n")
        self.f.write(
            "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n\n")

        self.f.write("%s:YHierarchy rdfs:range %s:HeaderRow ;\n" %
                     (PREFIX, PREFIX))
        self.f.write("\t\trdfs:domain %s:HeaderRow .\n" % PREFIX)
        self.f.write("%s:XHierarchy rdfs:range %s:HeaderColumn ;\n" %
                     (PREFIX, PREFIX))
        self.f.write("\t\trdfs:domain %s:HeaderColumn .\n" % PREFIX)

        self.f.write("%s:closestYHeaderCell rdfs:range %s:DataCell ;\n" %
                     (PREFIX, PREFIX))
        self.f.write("\t\trdfs:domain %s:HeaderRow .\n" % PREFIX)
        self.f.write("%s:closestXHeaderCell rdfs:range %s:DataCell ;\n" %
                     (PREFIX, PREFIX))
        self.f.write("\t\trdfs:domain %s:HeaderColumn .\n" % PREFIX)

        self.f.write("%s:belongs_to rdfs:range %s:Sheet ;\n" %
                     (PREFIX, PREFIX))
        self.f.write("\t\trdfs:domain %s:Dataset .\n" % PREFIX)

        self.f.write("%s:belongs_to rdfs:range %s:HeaderColumn ;\n" %
                     (PREFIX, PREFIX))
        self.f.write("\t\trdfs:domain %s:Sheet .\n" % PREFIX)

        self.f.write("%s:belongs_to rdfs:range %s:HeaderRow ;\n" %
                     (PREFIX, PREFIX))
        self.f.write("\t\trdfs:domain %s:Sheet .\n" % PREFIX)

        self.f.write("%s:belongs_to rdfs:range %s:DataCell ;\n" %
                     (PREFIX, PREFIX))
        self.f.write("\t\trdfs:domain %s:Sheet .\n\n" % PREFIX)

        file_uri = uri('F', dataset.dataset_id())
        self.f.write('%s %s %s ;\n' % (
            file_uri,
            'rdf:type',
            type_uri('Dataset')
        ))
        self.f.write('\t\t%s %s ;\n' % (
            predicate('downloaded_date'),
            '"%s"' % dataset.downloaded_date
        ))
        self.f.write('\t\t%s %s ;\n' % (
            predicate('url'),
            '"%s"' % dataset.url
        ))
        self.f.write('\t\t%s %s ;\n' % (
            predicate('downloaded_file_path'),
            '"%s"' % dataset.downloaded_file_path
        ))
        self.f.write('\t\t%s %s .\n\n' % (
            predicate('description'),
            '"%s"' % normalized_text(dataset.description)
        ))

        return file_uri

    def __write_headers(self, hiearchy, dict_header_cells,
                        cell_id_prefix, hierarchy_type):
        cell_id_prefix = get_uri_prefix(self.sheet_uri) + '_' + cell_id_prefix
        for child_header_id, parent_header_id in hiearchy.items():
            self.write_header_cell(self.f,
                                   uri(cell_id_prefix, child_header_id),
                                   dict_header_cells[child_header_id],
                                   hierarchy_type,
                                   uri(cell_id_prefix, parent_header_id) if parent_header_id is not None else None)

    def write_header_rows(self, hierarchy, dict_header_cells):
        self.__write_headers(hierarchy, dict_header_cells, 'HR', 'YHierarchy')

    def write_header_columns(self, hiearchy, dict_header_cells):
        self.__write_headers(hiearchy, dict_header_cells, 'HC', 'XHierarchy')

    def write_data_cell(self, f, _id, cell_value, x, y, header_x_uri, header_y_uri):
        f.write('%s %s %s ;\n' % (
            uri(get_uri_prefix(self.sheet_uri) + '_' + 'DC', _id),
            predicate('value'),
            '"%s"' % cell_value if cell_value is not None else '"None"'
        ))
        f.write('\t\t%s %s ;\n' % (
            predicate('belongs_to'),
            self.sheet_uri
        ))
        f.write('\t\t%s %s ;\n' % (
            'rdf:type',
            type_uri('DataCell')
        ))
        f.write('\t\t%s %s ;\n' % (
            predicate('posX'),
            x
        ))
        f.write('\t\t%s %s ;\n' % (
            predicate('posY'),
            y
        ))
        f.write('\t\t%s %s ;\n' % (
            predicate('closestXCell'),
            header_x_uri
        ))
        f.write('\t\t%s %s .\n\n' % (
            predicate('closestYCell'),
            header_y_uri
        ))

    def write_data_rows(self, data_rows, last_row_cell_ids, last_column_cell_ids):
        first_header_column_uris = [
            uri(get_uri_prefix(self.sheet_uri) + '_HC', _id)
            for _id in last_column_cell_ids]
        first_header_row_uris = [
            uri(get_uri_prefix(self.sheet_uri) + '_HR', _id)
            for _id in last_row_cell_ids]

        data_cell_id = 0
        for x, data_row in enumerate(data_rows):
            for y, cell_value in enumerate(data_row):
                self.write_data_cell(self.f, data_cell_id, cell_value,
                                     x, y, first_header_column_uris[x], first_header_row_uris[y])
                data_cell_id += 1
        self.f.close()


if __name__ == '__main__':
    rdf_converter = RdfConverter('test.ttl',
                                 DataSet('url', 'downloaded_date',
                                         'downloaded_file_path', 'description'),
                                 Sheet(0, 'title', 'comment',
                                       'test.ttl', 'published_date'))
    rdf_converter.write_header_rows(
        {0: 2, 1: 2, 2: None},
        {0: 'A', 1: 'B', 2: 'C'},
    )

    rdf_converter.write_header_columns(
        {0: 2, 1: 2, 2: None},
        {0: 'd', 1: 'e', 2: 'f'},
    )

    rdf_converter.write_data_rows(
        [
            ['one', 'two'],
            ['three', 'four']
        ],
        [0, 1],
        [0, 1]
    )
